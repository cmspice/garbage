--boxed: array of pointers to values on the heap
--storable: array of values on the heap
--unboxed:


{-# LANGUAGE FlexibleContexts #-}

module Chunk
(

) where


import Control.Monad.Primitive (PrimMonad, PrimState)
import Data.Vector.Mutable as M
import Data.Word
import GHC.Generics (Generic)
import Linear

type BlockPos = V3 Int  

data Player = {
    mPlayerName :: String
}

data Block = { mBlockType :: Word8 }

data Spot = Entity Player | Entity Block 

data Chunk = Chunk {
    blocks::M.Vector Spot

}

main = putStrLn "hi"
