{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE MagicHash #-}

module Main where

import Control.Monad
import           Data.Proxy
import           GHC.TypeLits
import GHC.Prim
import           Prelude

data Wrap (a :: Nat) = Wrap

f1 :: (a <= b) => Wrap a -> Wrap b -> Wrap a
f1 = const

-- test transitivity of <=
-- gives error `Could not deduce: (a <=? c) ~ 'True arising from a use of ‘f1’`
--f2 :: (a <= b, b <= c) => Wrap a -> Wrap b -> Wrap c -> Wrap a
--f2 a b c = f1 a c

boop :: (m <= n) => Proxy m -> Proxy n -> ()
boop = undefined

-- gives error `Could not deduce: ((n - 1) <=? n) ~ 'True`
--goop :: forall n. (1 <= n) => ()
--goop = boop wm wn where
--  wn = Proxy :: Proxy n
--  wm = Proxy :: Proxy (n - 1)

-- just curious what happens when you try and put bottom in a bottomless type
myId :: a -> a
myId = undefined

-- gives error `Couldn't match a lifted type with an unlifted type`
--main :: IO ()
--main = void $ return $ myId (1 :: Int#)



-- typed iteration example (asked on reddit)
-- https://www.reddit.com/r/haskell/comments/ewrfaw/monthly_hask_anything_february_2020/fixmrnf?utm_source=share&utm_medium=web2x
-- TBH, where you actually need this, you can just iterate on a type level list and accumulate the index I guess
untypedFunc :: Int -> Int
untypedFunc = id

untypedIterate :: (Monoid m) => Int -> Int
untypedIterate n = sum . map untypedFunc $ [0..n]

typedFunc :: forall n. (KnownNat n) => Proxy n -> Int
typedFunc p = fromIntegral $ natVal p

typedIterate :: Proxy n -> Int
typedIterate = undefined -- ??
