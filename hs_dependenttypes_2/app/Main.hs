-- https://www.schoolofhaskell.com/user/konn/prove-your-haskell-for-great-safety/dependent-types-in-haskell

{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds, TypeFamilies, TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE StandaloneDeriving #-}

module Main where

import           Prelude hiding (init, tail, head, last, zipWith, replicate)

data Nat = Z | S Nat

infixl 6 :+

type family   (n :: Nat) :+ (m :: Nat) :: Nat
type instance 'Z     :+ m = m
type instance ('S n) :+ m = 'S (n :+ m)

infixl 7 :*
type family (n :: Nat) :* (m :: Nat) :: Nat
type instance 'Z :* m = 'Z
type instance ('S n) :* m = m :+ (n :* m)

type family Min (n :: Nat) (m :: Nat) :: Nat
type instance Min 'Z m = 'Z
type instance Min n 'Z = 'Z
type instance Min ('S n) ('S m) = 'S (Min n m)

data Vector n a where
  Nil  :: Vector Z a
  (:-) :: a -> Vector n a -> Vector ('S n) a
infixr 5 :-

-- equivalent definition using ExistentialQuantification
--data Vector a m
--  = (m ~ Z)   => Nil
--  | forall n. (m ~ S n) => (:-) a (Vector a n)


deriving instance Eq a => Eq (Vector n a)
deriving instance Show a => Show (Vector n a)

toList :: Vector n a -> [a]
toList Nil = []
toList (x :- xs) = x : toList xs

--fromList ::  -> [a] -> Vector a n
--fromList _ [] = Nil
--fromList _ _ = undefined
--fromList _ [] = undefined
--fromList _ (x:xs) = x :- fromList xs



init :: Vector ('S n) a -> Vector n a
init (x :- xs@(_ :- _)) = x :- init xs
init (x :- Nil) = Nil

head :: Vector ('S n) a -> a
head (x :- _) = x

tail :: Vector ('S n) a -> Vector n a
tail (_ :- xs) = xs

last :: Vector ('S n) a -> a
last (x :- Nil) = x
last (x :- xs@(_ :- _)) = last xs

append :: Vector n a -> Vector m a -> Vector (n :+ m) a
append Nil ys = ys
append (x :- xs) ys = x :- append xs ys
-- note we have
--xs :: Vector nm a
--ys :: Vector m a
--x :- append xs ys :: Vector ('S (nm :+ m)) a
--                  :: Vector (('S nm) :+ m)) a
--                  :: Vector (n :+ m)) a


instance Functor (Vector n) where
  fmap :: (a -> b) -> Vector n a -> Vector n b
  fmap f (x :- Nil) = f x :- Nil
  fmap f (x :- xs@(_ :- _)) = f x :- fmap f xs

uncons :: Vector ('S n) a -> (a, Vector n a)
uncons (x :- xs) = (x, xs)

zipWithSame :: (a -> b -> c) -> Vector n a -> Vector n b -> Vector n c
zipWithSame f Nil Nil = Nil
zipWithSame f (x :- Nil) (y :- Nil) = f x y :- Nil
zipWithSame f (x :- xs@(_ :- _)) (y :- ys@(_ :- _)) = f x y :- zipWithSame f xs ys

zipWith :: (a -> b -> c) -> Vector n a -> Vector m b -> Vector (Min n m) c
zipWith f Nil _ = Nil
zipWith f _ Nil = Nil
zipWith f (x :- xs@(_ :- _)) (y :- ys@(_ :- _)) = f x y :- zipWith f xs ys

data SNat (n :: Nat) where
  SZ :: SNat 'Z
  SS :: SNat n -> SNat ('S n)

-- Note that it is implemented as a data family instance in the singletons library
-- PolyKinds adds kind level polymorphism which I guess is needed for the data family (works on many kinds, not just *)

--data family Sing (a :: k) -- from Data.Singletons
--data instance Sing (n :: Nat) where
--  SZ :: Sing Z
--  SS :: Sing n -> Sing (S n)
--type SNat (n :: Nat) = Sing n

infixl 6 %:+
(%:+) :: SNat n -> SNat m -> SNat (n :+ m)
SZ   %:+ m = m
SS n %:+ m = SS (n %:+ m)

infix 7 %:*
(%:*) :: SNat n -> SNat m -> SNat (n :* m)
SZ %:* m = SZ
(SS n) %:* m = m %:+ (n %:* m)

sMin :: SNat n -> SNat m -> SNat (Min n m)
sMin SZ _ = SZ
sMin _ SZ = SZ
sMin (SS n) (SS m) = SS (sMin n m)

replicate :: SNat n -> a -> Vector n a
replicate SZ x = Nil
replicate (SS m) x = x :- replicate m x

sLength :: Vector n a -> SNat n
sLength Nil = SZ
sLength (_ :- xs) = SS (sLength xs)

class SingRep n where
  sing :: SNat n

instance SingRep 'Z where
  sing = SZ

instance SingRep n => SingRep ('S n) where
  sing = SS (sing :: SNat n)

replicate' :: forall n a. (SingRep n) => a -> Vector n a
replicate' = replicate (sing :: SNat n)


data SingInstance (n :: Nat) where
  SingInstance :: SingRep n => SingInstance n

singInstance :: SNat n -> SingInstance n
singInstance SZ     = SingInstance
singInstance (SS n) =
  case singInstance n of
    SingInstance -> SingInstance

transpose :: SingRep n => Vector m (Vector n a) -> Vector n (Vector m a)
transpose Nil = replicate' Nil
transpose (Nil :- _) = Nil
transpose ((x :- xs) :- xss) =
  -- singInstance witnesses the SingRep constraint
  case singInstance (sLength xs) of
    SingInstance -> (x :- fmap head xss) :- transpose (xs :- fmap tail xss)


-- or use smart constructor version which carries dictionary info
sZ :: SNat Z
sZ = SZ
sS :: SNat n -> SNat (S n)
sS n = case singInstance n of SingInstance -> SS n

-- define type level sets of numbers up to a certain number
data Ordinal (n :: Nat) where
  OZ :: Ordinal (S n)
  OS :: Ordinal n -> Ordinal (S n)

-- TODO fix me
ordinalPlus :: Ordinal n -> Ordinal m -> Ordinal (n :+ m)
ordinalPlus OZ OZ = OZ
ordinalPlus OZ (OS m) = OS $ ordinalPlus OZ m
ordinalPlus (OS n) m = OS $ ordinalPlus n m

sIndex :: Ordinal n -> Vector n a -> a
sIndex OZ     (x :- _)  = x
sIndex (OS n) (_ :- xs) = sIndex n xs

sElemIndices :: Eq a => a -> Vector n a -> [Ordinal n]
sElemIndices _ Nil = []
sElemIndices a (x :- xs) = OZ : (fmap OS $ sElemIndices a xs)




main :: IO ()
main = do
  print $ head (1 :- 2 :- Nil)
  print $ tail (1 :- 2 :- Nil)
  print $ sIndex (OS OZ) (0 :- 1 :- 2 :- 3 :- Nil)
  print $ sIndex OZ (0 :- 1 :- Nil)
