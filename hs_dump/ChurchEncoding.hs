

{-# LANGUAGE RankNTypes, RecordWildCards      #-}

import Control.Applicative

newtype ListC a =
    ListC {
      foldC :: forall r. (a -> r -> r) -> r -> r
    }

nilC = ListC (\f a -> a)

consC :: a -> ListC a -> ListC a
consC x xs = ListC fol where
	fol f a = f x (foldC xs $ f a)

isNil ListC{..} = foldC (\x a -> True) False == False

uncons :: (a -> ListC a -> r) -> r -> ListC a -> r
uncons f n l@ListC{..} = if isNil l then n else foo where
	foo = f x xs  
	(x:xs)= drop 1 $ foldC (:) undefined
-- TODO

foldC' :: (a -> r -> r) -> r -> ListC a -> r
foldC' co ni (ListC f) = f co ni

instance Functor ListC where
    fmap f = foldC' (\x xs -> consC (f x) xs) nilC


main = undefined