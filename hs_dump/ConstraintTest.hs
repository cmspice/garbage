{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE AllowAmbiguousTypes     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}


-- TODO finish this, I'm trying to test if a constraint that should have been established by a previous function use carries over to the next function

class Foo x y where
	foo :: x -> String
	foo = "foo"

class Bar x y where
	bar :: x -> String
	bar = "bar"


data A B = A
instance Foo A ()
instance Bar A ()

func1 :: (Foo x y, Bar x y) => x -> x
func1 = id
func2 :: (Bar x y) => x -> x
func2 = id