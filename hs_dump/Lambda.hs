--fac n = (\x -> x x) (\f n -> if n == 0 then 1 else n * f.f $ n -1)
--sad face (\x -> x x) is of type a -> a -> a -> a ... infinite type not supported in haksell :(

fix f = let x = f x in x
fac :: Integer -> Integer
fac = fix (\f n -> if n == 0 then 1 else n * f (n-1))

main = do
	print $ fac 5

