import Control.Monad

list1 = [(i,j,k) | i <- [1,2,3], j <- [1,2,3], k <-[0], even $ i + j + k]

list2 = do
	i <- [1,2,3]
	j <- [1,2,3]
	k <- [0]
	guard (even $ i + j + k)
	[(i,j,k)]

-- remember where are mapping the rhs of >>= over its lhs 
-- and the resulting value is concat of all the internal values
-- i.e. xs >>= f = concat (map f xs)
list3 = 
	[1,2,3] >>= 
	\i -> [1,2,3] >>=
	\j -> [0] >>=
	\k -> (if even $ i + j + k 
		then pure ()
		else mempty) >>
	[(i,j,k)]

-- were saying
-- lift (,,) to type [a -> a -> a -> (a,a,a)]
-- then applicative apply it to arguments
list4 = (,,) <$> [1,2,3] <*> [1,2,3] <*> [0]

main = do
	putStrLn $ show list1
	putStrLn $ show list2
	--fail "test fail"
	putStrLn $ show list3
	putStrLn $ show list4