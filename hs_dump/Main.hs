import Control.Lens

maze :: [[Int]]
maze = 
	[
	[0,0,0,1,1,1,1,0,1,0],
	[1,1,0,1,1,0,0,0,1,0],
	[0,0,0,1,1,0,1,1,1,0],
	[1,1,0,1,0,0,0,0,0,0],
	[1,0,0,1,0,1,1,1,1,0],
	[1,0,1,1,0,1,1,1,1,0],
	[1,0,1,1,0,0,0,0,0,0],
	[0,0,0,0,0,1,1,1,1,0],
	[1,1,0,1,1,1,1,1,1,0],
	]

mazeAt maze x y = maze !! y !! x
newMaze maze x y v = m.~
continueMaze (x,y) maze = map (\(a,b) m -> if mazeAt m a b == 0 then m.~)

solveMaze maze = 



main = do