import Control.Applicative

-------------
--monad laws notes
-------------
--join . fmap join = join . join
	--assosciativity for joins (i.e. doesn't matter if we collapse inner or outer monad first)
--join . fmap return = join . return = id
	--join is left inverse of return. 
	--return is NOT an inverse of join. 
	--Also note that the first 2 expressions in the law have different domains.
--return . f = fmap f . return
--join . fmap (fmap f) = fmap f . join
	--I guess this is like saying return/join commute with fmap




-------------
--actual code below here that's unrelated
-------------



mainDo = do 
	line1 <- getLine
	line2 <- getLine
	line3 <- getLine
	putStrLn $ line1 ++ line2 ++ line3
	putStrLn $ line1 ++ line2 ++ line3


--(>>) :: Monad m => m a -> m b -> m b
--(>>=) :: (Monad m) => m a -> (a -> m b) -> m b  
--note that each getLine is independent of the results of the previous, but there is no way for Haskell to know this from its type
mainBind = getLine >>= 
	(\line1 -> getLine >>= 
		(\line2 -> getLine >>= 
			(\line3 -> putStrLn(line1 ++ line2 ++ line3) >>
				putStrLn(line1 ++ line2 ++ line3))))

--(<*>) :: Applicative f => f (a -> b) -> f a -> f b
--applicative, note how it is easy to see the getLines are independent of each other's results
mainApplicative = pure (\line1 line2 line3 -> line1 ++ line2 ++ line3) <*> getLine <*> getLine <*> getLine >>= (\x -> putStrLn x)


--(fmap) (<$>) :: Functor f => (a -> b) -> f a -> f b 
--think of >>= as linear continution, and <*> as a fork
mainCombined = do 
	--lines <- (++) <$> getLine <*> getLine
	lines <- (\x y z ->x++y++z) <$> getLine <*> getLine <*> getLine
	putStrLn lines

main = mainBind