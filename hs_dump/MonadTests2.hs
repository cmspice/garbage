-- http://book.realworldhaskell.org/read/monad-transformers.html
-- file: ch18/CountEntries.hs
--module CountEntries (listDirectory, countEntriesTrad) where

import System.Directory (doesDirectoryExist, getDirectoryContents)
import System.FilePath ((</>))
import Control.Monad (forM, forM_, liftM, when)
import Control.Monad.Trans (liftIO)
import Control.Monad.Writer (WriterT, tell, runWriterT)

import Data.Functor


import System.Environment

listDirectory :: FilePath -> IO [String]
listDirectory = liftM (filter notDots) . getDirectoryContents
    where notDots p = p /= "." && p /= ".."

countEntriesTrad :: FilePath -> IO [(FilePath, Int)]
countEntriesTrad path = do
  contents <- listDirectory path
  rest <- forM contents $ \name -> do
            let newName = path </> name
            isDir <- doesDirectoryExist newName
            if isDir
              then countEntriesTrad newName
              else return []
  return $ (path, length contents) : concat rest

countEntries :: FilePath -> WriterT [(FilePath, Int)] IO String
countEntries path = do
  contents <- liftIO . listDirectory $ path
  tell [(path, length contents)]
  forM_ contents $ \name -> do
    let newName = path </> name
    isDir <- liftIO . doesDirectoryExist $ newName
    if isDir
        then countEntries newName
        else return "END"
    --when isDir $ countEntries newName
  return "RECURSIVE"


main = do 
  (w,val) <- runWriterT $ do
    (x:xs) <- liftIO getArgs
    --x <- return "./"
    countEntries x
  putStrLn.show $ w
  putStrLn.concat $ (\x -> show x ++ "\n") <$> val

	--(x:xs) <- liftIO getArgs
  --countEntries  "./"
	--paths <- countEntries x
  --y <- liftIO.return $ x
  --return 1

  --liftIO putStrLn $ "aoeu"
	--putStrLn.concat $ (\x -> show x ++ "\n") <$> paths