import Control.Monad 
import Control.Applicative

--This monad has no context otherwise we would have
data JokeMonad a = JokeMonad {
    contents :: a
}

instance Monad JokeMonad where 
    --(>>=) :: JokeMonad a -> (  a -> JokeMonad b) -> JokeMonad b
    f >>= g = JokeMonad $ contents.g $ (contents f) 
    --(>>) :: JokeMonad a -> JokeMonad b -> JokeMonad b
    f >> g = f >>= (\ _ -> g)
    --return :: a -> JokeMonad a
    return = JokeMonad

instance Functor JokeMonad where
    fmap f (JokeMonad x) = JokeMonad (f x)

instance Applicative JokeMonad where
    pure = JokeMonad
    JokeMonad f <*> JokeMonad x = JokeMonad (f x)


--A monad is a value that depends on a context object 
--and/or modifies a context object
data GenericMonad ctx a = GenericMonad {
    runMonad :: ctx -> (ctx, a)
}

instance Monad (GenericMonad c) where 
    f >>= g = GenericMonad $ func where
        func ctx = runMonad (g a) ctx2 where 
            (ctx2, a) = (runMonad f) ctx
    --f >> g = f >>= (\ _ -> g)
    return a = GenericMonad (\ctx -> (ctx, a))

instance Functor (GenericMonad c) where
    fmap f x = do 
        a <- x 
        return $ f a
    --fmap = liftM

instance Applicative (GenericMonad c) where
    pure = return
    mf <*> y = do
        f <- mf
        val <- y
        return $ f val
    --(<*>) = ap

main = do 
    undefined