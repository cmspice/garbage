import Text.Read (readMaybe)
import Data.Maybe
import Data.Char
import Data.Foldable
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Maybe

liftMaybe :: (Monad m) => Maybe a -> MaybeT m a
liftMaybe = MaybeT . return 

maybeMain :: MaybeT IO ()
maybeMain = do
	xs <- replicateM 3 $ lift getLine
	--guard (all (isDigit.head) xs)
	ys <- sequence $ map (liftMaybe.(readMaybe::String->Maybe Int)) xs
	--let ys = map (readMaybe::String->Maybe Int) xs
	--guard (isJust $ sequence ys)
	lift $ putStrLn.show $ sum ys

main = runMaybeT maybeMain