import Control.Monad
import Control.Monad.Trans.State
import System.Random
import System.Environment
import Text.Read

randomDice :: (RandomGen g, Random a) => a -> a -> Int -> State g [a]
randomDice min max n = replicateM n $ do
    state $ randomR (min, max) 

getRandom :: (RandomGen g, Random a) => State g a
getRandom = state $ random

allTypes :: (RandomGen g) => State g (Int, Float, Char, Integer, Double, Bool, Int)
allTypes = (,,,,,,) <$> getRandom
                    <*> getRandom
                    <*> getRandom
                    <*> getRandom
                    <*> getRandom
                    <*> getRandom
                    <*> getRandom

main = do 
    s <- getArgs
    case s of
        [] -> putStrLn "Error, must provide length"
        x:_ -> case readMaybe x of
            Nothing -> case readMaybe x of 
                -- whoops, this is silly, because n should always be an Int
                Nothing -> putStrLn "Error, not an integer or float" 
                Just n -> getStdGen >>= putStrLn.(show::[Float]->String).evalState (randomDice 1 6 n)
            --Just n -> putStrLn.show.fst.runState (randomDice n) $ (mkStdGen 0) 
            Just n -> getStdGen >>= putStrLn.(show::[Int]->String).evalState (randomDice 1 6 n)
    getStdGen >>= putStrLn.show.evalState allTypes

    
