import Data.STRef
import Control.Monad.ST
import Control.Monad

{-
newSTRef :: a -> ST s (STRef s a)
-s is anything and therefore always different. It is an "uninstantiated type variable",
but you can force it to be RealWorld by type inference with
stToIO :: ST RealWorld a -> IO a
-the internal state of an ST monad is a mapping of references (STRef) to values
-(though I think this has nothing to do with the s, other than the fact that another ST monad would not have the same mappings and this is represented by the s not being the same I guess)

readSTRef :: STRef s a -> ST s a
-pulls out the value from an st ref into the ST monad, 
but you can only do this in the ST monad that created it
because that's the only place where the s in both types are the same

writeSTRef :: STRef s a -> a -> ST s ()
modifySTRef :: STRef s a -> (a -> a) -> ST s ()

runST :: (forall s. ST s a) -> a
-}

a = newSTRef (10::Int)

main1 = print $ runST $ do
    ref <- newSTRef 0
    replicateM_ 1000000 $ modifySTRef ref (+1)
    readSTRef ref

main2 = print $ runST $ newSTRef (10::Int) >>= readSTRef

-- these are no good because s escapes its scope
-- ref = runST $ newSTRef (4 :: Int)
-- ref = runST $ readSTRef =<< runST (newSTRef (4 :: Int))

main = main2