{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}


data AnyTree :: * -> AnyTreeList * -> *

data AnyTreeList :: (*->*) -> AnyList * * -> *