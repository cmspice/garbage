import           Control.Monad               (replicateM_)
import           Data.Vector.Unboxed         (freeze, unsafeFreeze)
import           Data.Vector.Unboxed         ((!), (//))
import qualified Data.Vector.Unboxed         as V
import qualified Data.Vector.Unboxed.Mutable as V2
import           System.Random               (randomRIO)


--Note Data.Vector.Unboxed.Vector is NOT traversable. We have:
    --traverse :: Applicative f => (a -> f b) -> t a -> f (t b)
    --sequenceA :: Applicative f => t (f a) -> f (t a)
    --sequenceA = traverse id 
--but we don't have 
    --(Unbox a, Applicative f) => Unbox f a

--you could do this just as well with Data.Vector
mainImmutable :: IO ()
mainImmutable = do
    let v0 = V.replicate 10 (0 :: Int)
        loop v 0 = return v
        loop v rest = do
            i <- randomRIO (0, 9)
            let oldCount = v ! i
                v' = v // [(i, oldCount + 1)]
            loop v' (rest - 1)

    vector <- loop v0 (10^6)
    print vector


mainMutable :: IO ()
mainMutable = do
    vector <- V2.replicate 10 (0 :: Int)

    replicateM_ (10^6) $ do
        i <- randomRIO (0, 9)
        --consider replace with read/writeUnsafe, disables some bound checks but can segfault :o
        oldCount <- V2.read vector i
        V2.write vector i (oldCount + 1)

    ivector <- freeze vector
    print ivector

mainFreeze :: IO ()
mainFreeze = do
    vector <- V2.replicate 1 (0 :: Int)
    V2.write vector 0 1
    --this version makes a copy
    --ivector <- freeze vector
    --unsafe version does not copy
    ivector <- unsafeFreeze vector
    print ivector
    V2.write vector 0 2
    print ivector

main = mainFreeze
