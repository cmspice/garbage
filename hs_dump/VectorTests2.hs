import           Control.Monad
import           Data.Binary.Get
import qualified Data.ByteString              as B
import qualified Data.ByteString.Internal     as B
import qualified Data.ByteString.Unsafe       as B
import qualified Data.Vector.Storable.Mutable as M
import           Data.Word
import           Foreign.ForeignPtr
import           Foreign.Storable


--conversion operations copied from https://hackage.haskell.org/package/spool

fp = "values.txt"

main = do
    vec <- (M.unsafeNew 100)::IO (M.IOVector Word8)
    M.write vec 10 (123::Word8)
    let
        lb = sizeOf (undefined::Word8)
        l = M.length vec
    -- convert to bytestring unsafe
    -- this is really bad as "bs" holds no tracked reference to "vec". i.e. vec is only guaranteed to not be GC'd inside of the lambda passed into unsafeWith
    -- unsafePackCStringFinalizer expects to own the CString and will free it with the provided finalizer
    bs <- M.unsafeWith vec (\x -> B.unsafePackCStringFinalizer x (l * lb) (return ()))
    --putStrLn $ show bs
    B.writeFile fp bs


    -- s <- foldM (\a i -> M.read vec i >>= return.(a++).show) "" [0..(l-1)]
    -- writeFile fp s

    --read file
    file <- B.readFile fp
    let
        (fptr, off, len) = B.toForeignPtr file
        scale = (`div` lb)
        nv::M.IOVector Word8
        nv = M.unsafeFromForeignPtr(castForeignPtr fptr) (scale off) (scale len)
    s <- foldM (\a i -> M.read nv i >>= return.((a++" ")++).show) "" [0..(l-1)]
    putStrLn $ show s

    -- QUESTION: wouldn't "file" get GC'd at some point and which would break "nv"?
        -- answer: I think "fptr", "nv" and "file" are all have tracked references to the allocated data so won't get GC'd until all 3 are out of scope





