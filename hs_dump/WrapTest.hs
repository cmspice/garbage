{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE AllowAmbiguousTypes     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
class MyClass c o where
	foo :: c -> String

data A = A 
data B = B
data C = C

instance MyClass A o where
	foo a = "A"

instance MyClass B o where
	foo a = "B"

instance MyClass C o where
	foo a = "C"

data WrapMyClass o = forall c. MyClass c o => WrapMyClass c

instance forall o. MyClass (WrapMyClass o) o where
	foo (WrapMyClass a) = foo a

_A = WrapMyClass A :: WrapMyClass ()
_B = WrapMyClass B :: WrapMyClass ()
_C = WrapMyClass C :: WrapMyClass ()


main = do 
	putStrLn $ foo _A
	putStrLn $ foo _B
	putStrLn $ foo _C