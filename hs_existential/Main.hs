{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE LambdaCase                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}

module Main where


import           Data.Proxy
import           Data.Singletons
import           Data.Singletons.Prelude.List
import           Data.Singletons.TypeLits



data Showable = forall a. (Show a) => Showable a

-- does not work
{-
specialShow' :: forall a. (Show a) => a -> String
specialShow' (a :: Int) = "this is an int " ++ show a
specialShow' a          = show a

specialShow :: Showable -> String
specialShow (Showable a) = specialShow' a
-}

-- this works as expected
showShowable :: Showable -> String
showShowable (Showable a) = show a

-- also doesn't work
showSList :: forall hs. SList hs -> String
showSList = \case
  SNil -> "end"
  -- does not work
  --(SNat :: Sing h) `SCons` ss -> show (natVal (proxy :: Proxy h)) ++ ":" ++ showSing ss
  _ `SCons` ss -> "elem:" ++ showSList ss

-- so the KnownNat instance allows us to pattern match on SNat n
showSing :: forall hs. (SingI hs, KnownNat hs) => Sing hs -> String
showSing = \case
  -- SNat :: KnownNat n => Sing n
  (SNat :: Sing n) -> "SNat " ++ show (natVal (Proxy :: Proxy n))
  _ -> "Something else"

{- does not work, need an h in output type and this deduces the type hs or something like that I guess
showSNatSList :: forall h hs. (KnownNat h) => SList hs -> String
showSNatSList = \case
  SNil -> "end"
  (SNat :: Sing h) `SCons` ss -> show (natVal (Proxy :: Proxy h)) ++ ":" ++ showSNatSList ss
-}


data MyNat n where
  MyNat :: forall n. (KnownNat n) => Proxy n -> MyNat n

myNatFunc :: forall n. (KnownNat n) => MyNat n -> String
myNatFunc (MyNat (p :: Proxy n)) = show $ natVal (Proxy :: Proxy n)

--makeSomeNat :: forall n. (KnownNat n) => Int -> Vec

--('True ': 'True ': 'False ': '[])
main :: IO ()
main = do
  putStrLn $ showShowable (Showable (100 :: Int))
  putStrLn $ showShowable (Showable (100 :: Float))
  putStrLn $ showSList (sing :: SList '[])
  putStrLn $ showSing (sing :: SNat 100)
  putStrLn $ myNatFunc (MyNat (Proxy :: Proxy 123))
