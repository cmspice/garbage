#ifndef CALLERBACK_H
#define CALLERBACK_H

// prevent name mangling
#ifdef __cplusplus
extern "C" {
#endif


typedef double (d2d)(double);
double twice(d2d f, double x);

#ifdef __cplusplus
}
#endif

#endif
