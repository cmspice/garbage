{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Complex (
  Complex(..)
) where

import Foreign.C.Types
import Foreign.Ptr
import Foreign.Storable


-- can we do deriving (Storable) here?
data Complex = Complex CDouble CDouble deriving(Show)

instance Storable Complex where
    sizeOf _ = 2 * sizeOf (undefined :: CDouble) -- stored complex size = 2 * size of a stored Double
    alignment _ = 1
    peek ptr = do
        real <- (peek :: Ptr CDouble -> IO CDouble) (castPtr ptr)
        img  <- peekByteOff ptr (sizeOf real) -- we skip the bytes containing the real part
        return $ Complex real img
    poke ptr (Complex real img) = do
        (poke :: Ptr CDouble -> CDouble -> IO ()) (castPtr ptr) real
        pokeByteOff ptr (sizeOf real) img




newtype Pair = Pair Complex deriving (Storable)
