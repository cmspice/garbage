{-# LANGUAGE ForeignFunctionInterface #-}

module Lib (
  someFunc
) where

import qualified Data.Text as T

import Complex

import Foreign.C.Types
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal.Alloc
import Foreign.Storable

--foreign import ccall "exp" c_exp :: Double -> Double
-- use Foreign.C.Types instead (same deal)
foreign import ccall "exp" c_exp :: CDouble -> CDouble
-- does not actually have side effects but we say it does
foreign import ccall "exp" c_exp_io :: CDouble -> IO CDouble
-- get pointer to the function
foreign import ccall "&exp" a_exp :: FunPtr (Double -> Double)
-- https://hackage.haskell.org/package/base-4.12.0.0/docs/Foreign-Ptr.html
foreign import ccall "dynamic" mkFun :: FunPtr (Double -> Double) -> (Double -> Double)
foreign import ccall "wrapper" createAddPtr :: (Int -> Int) -> IO (FunPtr (Int -> Int))

-- look into https://hackage.haskell.org/package/base-4.12.0.0/docs/Foreign-StablePtr.html


-- TODO try and write your own finalizer (print potato emoji than call free)
--type FinalizerPtr a = FunPtr (Ptr a -> IO ())

someFunc :: IO ()
someFunc = do
  print $ T.pack "hi"
  print $ c_exp 2
  c_exp_io 2 >>= print

  -- dynamic
  print $ (mkFun a_exp) 2

  -- wrapper
  addPtr <- createAddPtr (+1)
  -- you can use addPtr like any other FunPtr (e.g. give it to foreign code)
  -- you MUST free the FunPtr, otherwise it won't be collected
  freeHaskellFunPtr addPtr

  -- allocate on haskell heap and freed when closure terminates
  allocaBytes 128 $ \ptr -> do
    return ()
    -- ptr is deallocated

  -- malloc
  -- seems like size and Ptr type are not related at the type level
  -- you could also use (sizeOf (undefined::CInt))
  malloc_ptr <- mallocBytes 128
  print =<< peek malloc_ptr
  --poke malloc_ptr (1::CInt)
  poke malloc_ptr (Complex 1 1)
  print =<< peek malloc_ptr
  free malloc_ptr

  -- ForeignPtr
  -- make ForeignPtr yourself from a Ptr
  malloc_ptr2 <- mallocBytes 128
  malloc_fptr2 <- newForeignPtr finalizerFree malloc_ptr2
  -- free is called when malloc_fptr is GCd

  -- more efficient than above bc GHC has optimized implementation
  malloc_fptr <- mallocForeignPtrBytes 128
  -- finalizer (free) is called when malloc_fptr is GCd


  return ()
