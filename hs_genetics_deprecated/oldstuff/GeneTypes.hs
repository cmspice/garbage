module GeneTypes
(
	DNA,
	Gene,
	Genotype,
	Phenotype,
	Species

	showGenes,
	compatible,
	breed,
) where

import System.Random

type Gene = (Bool, Bool) 

type DNA = [Gene] 

--Indices of DNA
type Genotype = [Int]

type Phenotype t = Genotype -> DNA -> t

data Species = Species 
{
	dna :: DNA,
	phenotypes :: [(String, Genotype, Phenotype)] 
}



showGenes :: Genotype -> DNA -> String
showGenes [] d = ""
showGenes (x:xs) d = (show $ d!!x) ++ "," ++ showGenes xs d

compatible :: DNA -> DNA -> Bool
compatible a b = length a == length b

breed :: RandomGen gen => gen -> DNA -> DNA -> DNA
breed g [] _ = []
breed g (x:xs) (y:ys) = 
	(pickGene rx x, pickGene ry y) : breed ng xs ys where
		pickGene rng (a1,a2) = if rng then a1 else a2
		rx :: Bool
		ry :: Bool
		(rx,ng') = random g
		(ry,ng) = random ng'

