{-# LANGUAGE FlexibleContexts #-}

import Data.Traversable (mapAccumL)

import Data.Vector.Unboxed.Mutable as M
import Data.Vector.Unboxed as V
import Data.Word
import Data.Bits

import System.Random
import GHC.Generics (Generic)


kAlleleOffset :: Int
kAlleleOffset = 8 

type Allele = Word8
type Gene = Word16 --two alleles, yes, we could have just done (Allele,Allele)
type DNA = V.Vector Gene

type MDNA s = M.MVector s Gene

leftAllele :: Gene -> Allele
leftAllele = fromIntegral.(flip.shiftR) kAlleleOffset.(flip.shiftL) kAlleleOffset

rightAllele :: Gene -> Allele
rightAllele = fromIntegral.(flip.shiftR) kAlleleOffset 

mateAllele :: Allele -> Allele -> Gene
mateAllele a b = fromIntegral (shiftL a kAlleleOffset) .|. fromIntegral b

breed :: RandomGen gen => gen -> DNA -> DNA -> DNA
breed g a b = mapAccumL choose g (V.zip a b) where
	choose gen (geneA, geneB) = (ng, mated) where
		fc :: Bool
		sc :: Bool
		(fc,ng') = random g
		(sc,ng) = random ng'
		--shift either left or right, but alway end up with XXXXXXXX00000000
		chooseShift bL = if bL then leftAllele else rightAllele
		mated = mateAllele ((chooseShift fc) geneA) ((chooseShift sc) geneB)

main = undefined

