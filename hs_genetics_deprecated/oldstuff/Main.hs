
-- A nifty animated fractal of a tree, superimposed on a background 
--	of three red rectangles.
import Graphics.Gloss
import GeneTypes

main :: IO ()
main 
 = 	animate (InWindow "Zen" (500, 500) (5, 5)) 
 			(greyN 0.2) 
 			frame	


-- Produce one frame of the animation.
frame :: Float -> Picture
frame timeS
 = Pictures	
 	-- the red rectangles
	[ Translate 0 150 	backRec
	, Translate 0 0		backRec
	, Translate 0 (-150)	backRec
	]


-- One of the red backing rectangles, with a white outline.
backRec :: Picture
backRec	
 = Pictures
	[ Color red 	(rectangleSolid 400 100)
 	, Color white	(rectangleWire  400 100) ]
