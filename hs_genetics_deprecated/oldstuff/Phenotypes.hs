import GeneTypes

import System.Random


--Helpers

type MendelGenotype = [Bool]
type MendelPhenotype t = MendelGenotype -> t

mendel :: Gene -> Bool
mendel (a,b) = a || b

mendelGenotype :: Genotype -> DNA -> MendelGenotype
mendelGenotype (x:xs) d = (mendel $ d!!x) : mendelGenotype xs d

toPhenotype :: MendelPhenotype t -> Phenotype t
toPhenotype mpt gt d = mpt (mendleGenotype gt d)


breed :: RandomGen gen => gen -> DNA -> DNA -> DNA