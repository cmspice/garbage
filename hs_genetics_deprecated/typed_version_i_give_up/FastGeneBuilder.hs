{-# LANGUAGE KindSignatures            #-} -- needed to explictly declare (n::Nat)
{-# LANGUAGE ExistentialQuantification #-} -- forall 
{-# LANGUAGE ExplicitNamespaces        #-} -- so I can import type (<=)
{-# LANGUAGE TypeOperators             #-} -- so I can do (m <= n) on types
{-# LANGUAGE DataKinds                 #-} -- so I can do data ___ (n::Nat)
{-# LANGUAGE GADTs                     #-} -- so I can do fancy type constraints?

module FastGeneBuilder (
    FastGTList(..),
    FastGeneBuilder,
    gbSum
) where 

import Gene
import Genotype

import           GHC.TypeLits        (Nat, type (<=))

import Control.Monad.State.Lazy
import Control.Monad.Identity



data FastGTList (n::Nat) = forall m. (m <= n) => ConsFastGTList (FastGenotype n) (FastGTList m) | EmptyFastGTList
--TODO cons operation
--TODO last operation
--TODO fold last gtlist to genotype of base DNA

--toRootGenotype EmptyFastGTList = FastGenotype 0 0 0

type FastGeneBuilderT (n::Nat) m = StateT (DNA, FastGTList n) m
type FastGeneBuilder (n::Nat) = FastGeneBuilderT n Identity


--state :: (s -> (a, s)) -> m a
gbSum :: (Monad m) => FastGeneBuilderT (n::Nat) m Int
gbSum = state gbSum' where
    gbSum' (dna, gtlist) = (0, (dna, gtlist))


