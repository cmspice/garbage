module Gene (
    Gene4,
    DNA,
    --MDNA,
    breed
) where 

import           Data.Bits
--import           Data.List
import qualified Data.Vector.Unboxed         as V
--import qualified Data.Vector.Unboxed.Mutable as M
import           Data.Word

import           System.Random

type Gene4 = Word8 --4 gene pairs
type DNA = V.Vector Gene4

--type MDNA s = M.MVector s Gene4

breed :: RandomGen gen => gen -> DNA -> DNA -> DNA
breed g a b = V.map choose (V.zip3 a b (V.generate (V.length a) id)) where
    rands = randoms g
    choose (geneA, geneB, n) = mated where
        choiceL :: Word8
        choiceL = foldl (\acc x -> shiftL acc 2 .|. if x then 0x01 else 0x02) 0x00 $ take 4 . drop (n*8) $ rands
        choiceR :: Word8
        choiceR = foldl (\acc x -> shiftL acc 2 .|. if x then 0x01 else 0x02) 0x00 $ take 4 . drop (n*8+4) $ rands
        --shiftBitAtIndex: left or right, word to shift, index to shift
        --e.g. shiftBitAtIndex True (01010101) 2 = (00001000)
        shiftBitAtIndex::Bool -> Word8 -> Int -> Word8
        shiftBitAtIndex l x i = if doShift then shifted else bits where
            shiftDir = if l then shiftL else shiftR
            doShift = (shiftL (if l then 0x02 else 0x01) i) .&. x == (0x00::Word8) --shift if we do not find a 1 in left position
            bits::Word8
            bits = x .&. (shiftL 0x03 i) --grab the desired bits in the desired spot
            shifted = shiftDir bits 1 --shift the bytes
        chosenL = foldl (\acc x -> acc .|. shiftBitAtIndex True (geneA .&. choiceL) x) 0x00 [2*x | x <-[0..3]]
        chosenR = foldl (\acc x -> acc .|. shiftBitAtIndex False (geneB .&. choiceR) x) 0x00 [2*x | x <-[0..3]]
        mated::Word8
        --mated = trace (show chosenR ++ " " ++ show chosenL) $ chosenL .|. chosenR
        mated = chosenL .|. chosenR