{-# LANGUAGE KindSignatures            #-} -- needed to explictly declare (n::Nat)
{-# LANGUAGE ExistentialQuantification #-} -- forall 
{-# LANGUAGE ExplicitNamespaces        #-} -- so I can import type (<=)
{-# LANGUAGE TypeOperators             #-} -- so I can do (m <= n) on types
{-# LANGUAGE DataKinds                 #-} -- so I can do data ___ (n::Nat)
{-# LANGUAGE GADTs                     #-} -- so I can do fancy type constraints?

module GeneBuilder (
    GTList(..),
    GeneBuilder,
    gbSum
) where 

import Gene
import Genotype

import           GHC.TypeLits        (Nat, type (<=))

import Control.Monad.State.Lazy
import Control.Monad.Identity


data GTList (n::Nat) = forall m gt. (Genotype n gt, m <= n) => ConsGTList gt (GTList m) | EmptyGTList
--TODO cons operation
--TODO last operation
--TODO fold last gtlist to genotype of base DNA

type GeneBuilderT (n::Nat) m = StateT (DNA, GTList n) m
type GeneBuilder (n::Nat) = GeneBuilderT n Identity


--state :: (s -> (a, s)) -> m a
gbSum :: GeneBuilder (n::Nat) Int
gbSum = state gbSum' where
	gbSum' (dna, gtlist) = undefined
    --gbSum' (dna, gtlist) = (geneSum dna gtlist, (dna, gtlist))


