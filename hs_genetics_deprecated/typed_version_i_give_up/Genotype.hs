{-# LANGUAGE AllowAmbiguousTypes       #-} -- defers type check until function is actually used, I don't really understand this
{-# LANGUAGE DataKinds                 #-} -- so I can do data ___ (n::Nat)
--{-# LANGUAGE ExistentialQuantification #-}
--{-# LANGUAGE ExplicitNamespaces        #-}
--{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-} -- needed for instance declaration, not sure why I need this
{-# LANGUAGE GADTs                     #-} -- so I can do fancy type constraints?
{-# LANGUAGE KindSignatures            #-} -- so i can do (n::Nat)
{-# LANGUAGE MultiParamTypeClasses     #-} -- so I can do a multiparameter type class
{-# LANGUAGE TypeOperators             #-}
--{-# LANGUAGE RankNTypes                 #-}
--{-# LANGUAGE InstanceSigs                 #-} -- idk
{-# LANGUAGE FunctionalDependencies                 #-} -- needed so we can do (KnownNat n) => ... in class Genotype
{-# LANGUAGE ScopedTypeVariables                 #-} -- needed to access n of type sig in geneLength

module Genotype (
    Genotype(..),
    FastGenotype(..),
    GeneralGenotype(..),
    combineFast
) where

import           Gene

import           Data.Constraint.Nat
import           Data.Proxy          (Proxy (..))
import           GHC.TypeLits        (Nat, natVal, KnownNat (..), type (<=))

import qualified Data.Vector.Unboxed as V 

class (KnownNat n) => Genotype n gt | gt -> n where
    geneLength :: gt -> Integer
    geneLength gt = natVal (Proxy :: Proxy n)
    geneSum :: DNA -> gt -> Int
    --combine :: (Genotype m gt2, m <= n) => gt -> gt2 -> gt2


data FastGenotype (n::Nat) = FastGenotype {
    startGene :: Int,
    geneCount :: Int
}

combineFast :: (m <= n) => FastGenotype n -> FastGenotype m -> FastGenotype m  
combineFast gt1 gt2 = 
    if startGene gt2 + geneCount gt2 > geneCount gt1 
        then error "inconsistency" 
        else FastGenotype (startGene gt1 + startGene gt2) (geneCount gt2)

instance (KnownNat n) => (Genotype n) (FastGenotype n) where
    geneSum dna gt = fromInteger . toInteger . V.sum . V.take (geneCount gt) . V.drop (startGene gt) $ dna

data GeneralGenotype (n::Nat) = GeneralGenotype {
    -- this wont type check length of list obv.
    indices :: V.Vector Int
}

instance (KnownNat n) => (Genotype n) (GeneralGenotype n) where
    geneSum dna = fromInteger . toInteger . V.sum . V.map (\i -> dna V.! i) . indices




    



