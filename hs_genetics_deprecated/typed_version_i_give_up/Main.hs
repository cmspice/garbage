{-# LANGUAGE DataKinds                 #-} -- needed for Nat kind literals I think

import Genotype
import Gene
--import GeneBuilder
import FastGeneBuilder



import qualified Data.Vector.Unboxed         as V
import           Data.Word
--import           GHC.Generics                (Generic)
import           System.Random



testgt1 :: FastGenotype 1
testgt1 = FastGenotype 1 1
testgt2 :: FastGenotype 2
testgt2 = FastGenotype 1 1
testgt3 :: FastGenotype 3
testgt3 = FastGenotype 1 1

--testGTList = ConsGTList testgt3 $ ((ConsGTList testgt2 $ (EmptyGTList :: GTList 0)))
testFastGTList = ConsFastGTList testgt3 $ ((ConsFastGTList testgt2 $ (EmptyFastGTList :: FastGTList 0)))



main = do
    let
        firstDNA = V.replicate 5 (0x00::Word8) --all recessive
        secondDNA = V.replicate 5 (0xFF::Word8) --all dominant
        thirdDNA = V.replicate 5 (0x77::Word8) --all mixed
        --gen = mkStdGen 0
    gen <- getStdGen
    putStrLn $ show $ secondDNA
    putStrLn $ show $ breed gen thirdDNA firstDNA
