--https://crypto.stanford.edu/~blynn/haskell/brain.html

module MNISTLoader
(
	mnist_test_read, mnistVectors, print_mnist_vector
) where

import Codec.Compression.GZip (decompress)
import qualified Data.ByteString.Lazy as BS
import Data.Functor
import System.Random
import Types
import Numeric.LinearAlgebra.HMatrix 

--using training images from http://yann.lecun.com/exdb/mnist/
trainImages = "train-images-idx3-ubyte.gz"
trainLabels = "train-labels-idx1-ubyte.gz"
shortImages = "t10k-images-idx3-ubyte.gz"
shortLabels = "t10k-labels-idx1-ubyte.gz"

render n = let s = " .:oO@" in s !! (fromIntegral n * length s `div` 256)

mnist_test_read = do
  s <- decompress <$> BS.readFile trainImages
  l <- decompress <$> BS.readFile trainLabels
  n <- (`mod` 60000) <$> randomIO
  putStr . unlines $
    [(render . BS.index s . (n*28^2 + 16 + r*28 +)) <$> [0..27] | r <- [0..27]]
  print $ BS.index l (n + 8)


--images, labels
mnist_samples :: IO (BS.ByteString,BS.ByteString)
mnist_samples = do
	s <- decompress <$> BS.readFile trainImages
	l <- decompress <$> BS.readFile trainLabels
	return (s,l)

indexVector i = replicate (fromIntegral i) 0 ++ [1] ++ replicate (9-(fromIntegral i)) 0

mnistVectors :: IO [NNTraining]
mnistVectors = do
	(s,l) <- mnist_samples
	let 
		sampleIndices = [0..50000]
		samples = [fromList [(/255) $ fromIntegral $ BS.index s (n*28^2 + 16 + y*28 + x) | x<-[0..27], y<-[0..27]] | n <- sampleIndices]
		--samples = [fromList [fromIntegral $ BS.index s (n*28^2 + 16 + y*28 + x) | x<-[0..27], y<-[0..27]] | n <- sampleIndices]
		labels = [fromList $ indexVector $ BS.index l (n+8) | n <- sampleIndices]
	return $ zip samples labels

print_mnist_vector v = do
	putStr . unlines $ [(render .(v !). (16 + r*28 +)) <$> [0..27] | r <- [0..27]]






