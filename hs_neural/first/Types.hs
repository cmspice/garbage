module Types
(
	R, NNTraining, 
	Network(Network), weights, biases, sizes,
	emptyNetwork, loadNetwork, saveNetwork
) where

import Data.Functor
import Data.List
import System.Random

import Codec.Compression.Zlib     (compress, decompress)
import Data.Binary                (Binary(..), encode, decode)
import Foreign.Storable           (Storable)
import qualified Data.ByteString.Lazy  as B

import Numeric.LinearAlgebra.HMatrix 


type R = Double --I thought this was in Numeric.LinearAlgebra but it's not?
type NNTraining = (Vector R, Vector R) --input, desired output

data Network = Network 
	{ sizes :: [Int]
	, weights :: [Matrix R]
	, biases :: [Vector R] } deriving (Show)

instance Binary Network where 
	put network = put (sizes network, weights network, biases network)
  	get = do 
  		(s,w,b) <- get  
  		return Network{
  			sizes = s,
  			weights = w,
  			biases = b
  		}

emptyNetwork :: RandomGen a => [Int] -> a -> Network
emptyNetwork _sizes _gen =
	let
		dropSizes = Data.List.drop 1 _sizes
		rnList = randomRs (-1,1) _gen :: [R]
		weightPairs = zip _sizes dropSizes 
	in
		Network {
			sizes = _sizes,
			--TODO this initializes each layer of bias and weights with same random numbers, need to zip up how many to drop from rnList as well
			--but I'm leavign it as is because it doesn't really matter
			weights = Data.List.map (\(a,b)-> reshape a (fromList $ Data.List.take (a*b) rnList)) weightPairs,
			biases = Data.List.map (\a -> fromList $ Data.List.take a rnList) dropSizes
		}

numberLayers :: Network -> Int
numberLayers (Network {sizes = s}) = (Prelude.length s) - 2




-- | Loading a neural network from a file (uses zlib compression on top of serialization using the binary package).
--   Will throw an exception if the file isn't there.
loadNetwork :: FilePath -> IO (Network)
loadNetwork fp = decode . decompress <$> B.readFile fp

-- | Saving a neural network to a file (uses zlib compression on top of serialization using the binary package).
saveNetwork :: FilePath -> Network -> IO ()
saveNetwork fp = B.writeFile fp . compress . encode

