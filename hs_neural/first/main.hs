--NOTES, macports version of libiconv is incompatable
--see http://blog.omega-prime.co.uk/?p=96
--run ghc main -L/usr/lib

--TODO figure out what all these mean, need it in order for activation function you copied to work
{-# LANGUAGE BangPatterns, 
             ScopedTypeVariables,
             RecordWildCards,
             FlexibleContexts,
             TypeFamilies #-}


import System.Random
import System.IO
import Data.List
import Numeric.LinearAlgebra.HMatrix 
import Debug.Trace
import System.Environment
import Data.Ord
import Data.Double.Conversion.ByteString

import Types
import MNISTLoader

--outputs activations of each layer
scanFeedForward :: Network -> Vector R -> [Vector R]
scanFeedForward network input = 
	let 
		iterate = Data.List.zip (weights network) (biases network)
		fn a (w,b) = sigmoid (w #> a + b)
	in
		Data.List.scanl fn input iterate

feedForward :: Network -> Vector R -> Vector R
feedForward network input = 
	Data.List.last $ scanFeedForward network input 

groupList :: Int -> [a] -> [[a]]
groupList _ [] = []
groupList n l
  | n > 0 = (take n l) : (groupList n (drop n l))
  | otherwise = error "Negative n"

stochasticGradientDescent :: Network -> [NNTraining] -> (Int,Int,R) -> Network
stochasticGradientDescent network training (mbsize, epochs, learningRate) =
	let 
		trainGroups = take epochs $ groupList mbsize training
		foldfn (accum,i) train = (gradientDescent accum train learningRate,i+1)
	in
		fst $ foldl foldfn (network,0) trainGroups

gradientDescent :: Network -> [NNTraining] -> R -> Network 
gradientDescent _network _training rate = 
	let
		deltas = map (\x -> backPropogate _network x) _training
		avg = foldr (\(w1,b1) (w2,b2) -> (zipWith (+) w1 w2,zipWith (+) b1 b2)) (head deltas) (tail deltas)
		s = rate / fromIntegral(length _training) 
		deltaWeights = (map (scale s) (fst avg))
		deltaBiases = (map (scale s) (snd avg))
	in
		trace (show $ deltaCost _network _training) $
		trace (show $ correct _network _training) $
		Network{
			sizes = sizes _network,
			weights = zipWith (-) (weights _network) deltaWeights,
			biases = zipWith (-) (biases _network) deltaBiases
		}

--this is the same as folding stochasticgradientdescent over training samples
onlineTraining  :: Network -> [NNTraining] -> Double -> Network
onlineTraining _network _training rate = 
	let
		foldfn accum train = 
			trace (show $ deltaCost accum [train]) $
			Network{
				sizes = sizes _network,
				weights = zipWith (-) (weights accum) (map (scale rate) (fst rslt)),
				biases = zipWith (-) (biases accum) (map (scale rate) (snd rslt))
			}
			where rslt = backPropogate accum train 
	in foldl foldfn _network _training

deltaCost :: Network -> [NNTraining] -> R
deltaCost network training = 
	(/(fromIntegral $ length training)).sum $ zipWith (\x y->(^2).norm_2 $ ((snd x)-y)) training outputs
	where
		outputs = map (\x->feedForward network (fst x)) training

correct :: Network -> [NNTraining] -> R
correct network training = 
	--(/(fromIntegral $ length training)).
	sum $ zipWith (\(a,b) y-> if maxIndex b == maxIndex y then 1 else 0) training outputs
	where
		outputs = map (\x->feedForward network (fst x)) training	

--computer C derivatives (assuming sigmoid) w.r.t weights and biases
backPropogate :: Network -> NNTraining -> ([Matrix R],[Vector R]) 
backPropogate network training = 
	let 
		_L = Data.List.length (sizes network) - 1 --indexing starts at 0!
		_weights =  [0] ++ (weights network) --append zero at the beginning to fix indexing
		_biases =  [0] ++ (biases network) 
		_Z l = (_weights !! l) #> (_A (l-1)) + (_biases !! l) --z at layer l
		_A 0 = fst training
		_A l = sigmoid (_Z l) --activations at layer l
		dCdZ l 
			| l == _L =  ((_A _L) - (snd training)) * (sigmoid' (_Z _L))
			| otherwise = 
				let 
					weightMatrix = (_weights !! (l+1))
					dAldZl = sigmoid' (_Z l)
				in
					((tr weightMatrix) #> dCdZ (l+1)) * dAldZl
	in
		(Data.List.map (\i->dCdZ i `outer` _A (i-1)) (take (_L) [1..]), Data.List.map (\i->dCdZ i) (take (_L) [1..]))

test_network_serialization = do
	let 
		network = emptyNetwork [28*28,17,10] (mkStdGen 0)
	saveNetwork "network.bin" network
	rn <- loadNetwork "network.bin"
	putStrLn $ show $ (show network) == (show rn)

train fn = do
	samples <- mnistVectors
	let 
		network = emptyNetwork [28*28,100,10] (mkStdGen 0)
		trained = stochasticGradientDescent network (cycle samples) (30,5000,1)
	saveNetwork fn trained

run fn = do 
	--let network = emptyNetwork [28*28,17,10] (mkStdGen 0)
	network <- loadNetwork fn
	samples <- mnistVectors
	let 
		showVec v = show $ map (toFixed 2) $ toList v
		showrslt test = putStrLn $ "\n\n" ++ (showVec $ snd test) ++ "\n" ++ (showVec (feedForward network $ fst test)) 
	mapM showrslt (take 100 samples)
	return ()

testtrainxor = do
	let 
		network = emptyNetwork [2,3,1] (mkStdGen 0)
		training = map (\(x,y)->(fromList x, fromList y)) xor_samples
		--samples = take 10000 $ cycle training
		--trained = onlineTraining network samples 1
		samples = cycle training
		trained = stochasticGradientDescent network samples (10,300,10)
		orig = map (\x->feedForward network (fst x)) training
		result  = map (\x->feedForward trained (fst x)) training
	--putStrLn $ show orig 
	--putStrLn $ show network
	putStrLn $ show result

testffxor = do 
	let 
		training = map (\(x,y)->(fromList x, fromList y)) xor_samples
		result  = map (\x->feedForward super_xor_network (fst x)) training
	putStrLn $ show result
	

main = do
	s <- getArgs
	case s of
		["version"] -> putStrLn "0.1"
		"train":xs -> train (xs!!0)
		"run":xs -> run (xs!!0)
		"trainxor":xs -> testtrainxor
		"ffxor":xs -> testffxor
		_ -> putStrLn "usage: version train run trainxor ffxor"
 
    




--copied from https://github.com/alpmestan/hnn/blob/master/AI/HNN/FF/Network.hs
-- | The sigmoid function:  1 / (1 + exp (-x))
sigmoid :: Floating a => a -> a
sigmoid !x = 1 / (1 + exp (-x))
{-# INLINE sigmoid #-}

-- | Derivative of the sigmoid function: sigmoid x * (1 - sigmoid x)
sigmoid' :: Floating a => a -> a
sigmoid' !x = case sigmoid x of
  s -> s * (1 - s)
{-# INLINE sigmoid' #-}

-- | Derivative of the 'tanh' function from the Prelude.
tanh' :: Floating a => a -> a
tanh' !x = case tanh x of
  s -> 1 - s**2
{-# INLINE tanh' #-}

-- | The rectified linear activation function. This is a more "biologically
--   accurate" activation function that still retains differentiability.
reclu :: Double -> Double
reclu t = log (1 + exp t)

-- | The derivative of the rectified linear activation function is just the
--   sigmoid.
reclu' :: Double -> Double
reclu' = sigmoid


xor_samples = 
	[([0,0],[0]),
	([1,0],[1]),
	([0,1],[1]),
	([1,1],[0])]

xor_network = Network{
	sizes = [2,3,1],
	weights = [(3><2)[10,0,10,10,0,10],
	(1><3)[10,-20,10]],
	biases = [vector [-5,-15,-5],vector [-5]]
}

--same as above with weights and biases multiplied
super_xor_network = Network{
	sizes = sizes xor_network,
	weights = zipWith (+) (weights xor_network) (map (100*) $ weights xor_network),
	biases = zipWith (+) (biases xor_network) (map (scale 100) $ biases xor_network)
}

