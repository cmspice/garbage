
import System.IO
import System.Environment


build_vt_list :: [(Float,Float)] -> String -> [(Float,Float)]
build_vt_list vtlist line = 
	let 
		ln = words line
		key = ln !! 0
		x = read (ln !! 1) :: Float
		y = read (ln !! 2) :: Float
	in 
		case key of 
			"vt" -> vtlist ++ [(x,y)]
			_ -> vtlist

vt_list_to_string :: [(Float,Float)] -> String
vt_list_to_string vtlist = 
	foldl foldfunc "" vtlist
	where
		foldfunc str (x,y) = str ++ ",\n{" ++ show x ++ "," ++ show y ++ "}"

	
hGetLines :: Handle -> IO [String]
hGetLines fileHandle = do
   eof <- hIsEOF fileHandle
   if eof then return []
          else do line <- hGetLine fileHandle
                  rest <- hGetLines fileHandle
                  return (line:rest)

--converts uv coordinates to static c array
convert_uv filename = do
	putStrLn "starting"
	f <- openFile filename ReadMode
	putStrLn $ "opened file " ++ filename
	hGetLine f >>= putStrLn
	lines <- hGetLines f
	putStrLn $ "read " ++ show (length lines) ++ " lines"
	let
		vt = foldl build_vt_list [] lines
	writeFile "output.c" (vt_list_to_string vt)



main = do
	s <- getArgs
	case s of
		[x] -> convert_uv x
		_ -> putStrLn "usage: please provide filename to convert"
 
    

