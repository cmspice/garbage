
-----------------------
--thunks
-----------------------

-the '_' represents an unevaluated thunk e.g.
	Prelude> let x = 1 + 2 :: Int 
	Prelude> let y = x + 1 
	Prelude> :sprint x 
	x= _
	Prelude> :sprint y 
	y= _
-the + above is an unevaluated thunks with pointers to its values. In the case of y, it holds a reference to the unevualted thunk x

-seq evaluates to WHNF (first constructor). I'm guessing this means that it's good enough for pattern matching?
	Prelude Data.Tuple> let z = swap (x,x+1)
	Prelude Data.Tuple> :sprint z 
	z= _
	Prelude Data.Tuple> seq z ()
	()
	Prelude Data.Tuple> :sprint z
	z = (_,_)
	-- lets evaluate x
	Prelude Data.Tuple> seq x ()
	()
	Prelude Data.Tuple> :sprint z
    z = (_,3)

-example with lists
	let xs = map (+1) [1..10] :: [Int]
	Prelude> seq xs ()
	()
	--WHNF of []
	Prelude> :sprint xs
	xs = _ : _
	--evaluate the list, note length function looks like length (_:xs) = ... so the elements do not need to be evaluated
	Prelude> length xs
	10
	Prelude> :sprint xs
	xs = [_,_,_,_,_,_,_,_,_,_]


-----------------------
--sparks
-----------------------
-running ghc (-02 means 2 threads I think)
	ghc -O2 sudoku2.hs -rtsopts -threaded
-will output
	SPARKS: 1000 (1000 converted, 0 overflowed, 0 dud, 0 GC'd, 0 fizzled)
-converted: turned into real parallelism at runtime?
-dud: spark for an already evaluated thunk
-GC'd: spark found to be unneeded and GC'd 
-The expression was unevaluated at the time it was sparked but was later evaluated independently by the program. Fizzled sparks are removed from the spark pool.

-----------------------
--NFData (normal form data), deepseq
-----------------------

-NFData class is for data with normal form (can be evaluated fully with no unevaluated subexpressions)
	class NFData a where rnf :: a -> ()
		rnf a = a `seq` () --default implementation for convenience
-create your own NFData instances by applying rnf to children and `seq` them together
	data Tree a = Empty | Branch (Tree a) a (Tree a)
	instance NFData a => NFData (Tree a) where
		rnf Empty = ()
		rnf (Branch l a r) = rnf l `seq` rnf a `seq` rnf r
-forcing evaluation 
	deepseq :: NFData a => a -> b -> b
	deepseq a b = rnf a `seq` b
	force :: NFData a => a -> a
	force x = x `deepseq` x
-turn it into a strategy (see below)
	rdeepseq :: NFData a => Strategy a 
	rdeepseq x = rseq (force x)

-----------------------
--Strategy / Eval Monad
-----------------------

type Strategy a = a -> Eval a

--Eval is evaluation
runEval :: Eval a -> a

 --Also identity monad
instance Monad Eval where
  return  = Done
  m >>= k = case m of
              Done x -> k x

using :: a -> Strategy a -> a
using a s = runEval (s a)

--r0 performs *no* evaluation.
r0 :: Strategy a 

--rseq evaluates its argument to weak head normal form.
rseq :: Strategy a

--rdeepseq fully evaluates its argument.
rdeepseq :: NFData a => Strategy a

--rpar sparks its argument (for evaluation in parallel).
rpar :: Strategy a

--par strategy combinator
--e.g. rparWith rdeepseq
--e.g. rparWith r0 (does no evaluation)
rparWith :: Strategy a -> Strategy a


--list, tuples, etc
evalList :: Strategy a -> Strategy [a]
parList = evailList . rparWith
parMap strat f = withStrategy (parList strat) . map f
parListChunk :: Int -> Strategy a -> Strategy [a]
--for lazy lists, rolling buffer strategy, saves memory, only creates as many sparks as specified by first argument
evalBuffer :: Int -> Strategy a -> Strategy [a]
evalTuple2/3/4/5/... :: Strategy a -> Strategy b -> Strategy (a, b)

--Sequential function application. The argument is evaluated using the given strategy before it is given to the function.
($|) :: (a -> b) -> Strategy a -> a -> b
--Parallel function application. The argument is evaluated using the given strategy, in parallel with the function application.
($||) :: (a -> b) -> Strategy a -> a -> b
--Sequential function composition. The result of the second function is evaluated using the given strategy, and then given to the first function.
(.|) :: (b -> c) -> Strategy b -> (a -> b) -> a -> c
--Parallel function composition. The result of the second function is evaluated using the given strategy, in parallel with the application of the first function.
(.||) :: (b -> c) -> Strategy b -> (a -> b) -> a -> c


--(x `using` s) may be less defined than just (x) e.g.
print $ snd (1 `div` 0, "Hello!")
print $ snd ((1 `div` 0, "Hello!") `using` rdeepseq)


--Control.Parallel.Strategies works well with traversable
	traverse :: Applicative f => (a -> f b) -> t a -> f (t b)
	sequenceA :: Applicative f => t (f a) -> f (t a)

	evalTraversable :: Traversable t => Strategy a -> Strategy (t a)
	evalTraversable = traverse

	-- so we have something like sequenceA :: t (Strategy a) -> Strategy (t a)


-----------
--Par Monad
-----------


-----------------------
--ForkIO, MVars and Channels
-----------------------



-----------------------
--Control.Concurrent.Async
-----------------------

