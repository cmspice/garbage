module Main where

import Parse2

parseSomething :: String -> IO ()
parseSomething input = putStrLn $ case parseCSV input of
  Left x -> show x
  Right x -> show x


main :: IO ()
main = do
  parseSomething "line1\nline2\nline3\n"
  parseSomething "line1\r\nline2\nline3\n\rline4\rline5\n\r"
