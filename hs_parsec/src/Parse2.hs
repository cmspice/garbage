module Parse2
    ( parseCSV
    ) where

import Text.ParserCombinators.Parsec

-- he second tool is endBy. It's similar to sepBy, but expects the very last item to be followed by the separator. That is, it continues parsing until it can't parse any more content.
-- The first tool is the sepBy function. This function takes two functions as arguments: the first function parses some sort of content, while the second function parses a separator. sepBy starts by trying to parse content, then separators, and alternates back and forth until it can't parse a separator. It returns a list of all the content that it was able to parse.
csvFile = endBy line eol
line = sepBy cell (char ',')
cell = many (noneOf ",\n\r")

--eol = char '\n'

-- need try to look ahead otherwise will consume and fail on mismatch for multiple chars
eol =  try (string "\n\r")
    <|> try (string "\r\n")
    <|> string "\n"
    <|> string "\r"
    <?> "end of line"


parseCSV :: String -> Either ParseError [[String]]
parseCSV input = parse csvFile "(unknown)" input
