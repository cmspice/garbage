-- based on http://book.realworldhaskell.org/read/code-case-study-parsing-a-binary-data-format.html

{-# LANGUAGE InstanceSigs #-}   
import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Data.ByteString.Lazy as L
import Data.Char (isSpace)
import Control.Monad 
import Control.Applicative
import Data.List (intercalate, map)
import Debug.Trace

data PGMFormat = P2 | P5 | InvalidFormat deriving (Show, Read)

data PGMParser a = PGMParser {
    runParse :: L.ByteString -> Either String (a, L.ByteString)
}

runParser :: PGMParser a -> L.ByteString -> Either String (a, L.ByteString)
runParser = runParse

(===>) :: PGMParser a -> (a -> PGMParser b) -> PGMParser b
fp ===> sp = PGMParser func where
    func s = case (runParser fp) s of 
        Left e -> Left e
        Right (a, s2) -> runParser (sp a) (L8.dropWhile isSpace s2)

(===>&) :: PGMParser a -> PGMParser b -> PGMParser b
a ===>& b = a ===> (\ _ -> b)

--assumes input has been cleaned already
(==>) :: PGMParser a -> (a -> PGMParser b) -> PGMParser b
fp ==> sp = fp ===> \x -> cleanInput ===>& sp x

(==>&) :: PGMParser a -> PGMParser b -> PGMParser b
a ==>& b = a ==> (\ _ -> b)

instance Monad PGMParser where
    (>>=) = (==>)
    return :: a -> PGMParser a
    return a = PGMParser (\x -> Right (a, x))

instance Functor PGMParser where
    fmap = liftM

instance Applicative PGMParser where
    pure = return
    (<*>) = ap

instance MonadPlus PGMParser where
    mzero = PGMParser (\x -> Left "")
    mplus = (==>&)

instance Alternative PGMParser where 
    empty = mzero
    (<|>) = mplus


greyASCIIOrder :: String
greyASCIIOrder = 
    " .`-_':,;^=+/\"|)\\<>)iv%xclrs{*}I?!][1taeo7zjLu" ++ 
    "nT#JCwfy325Fp6mqSghVd4EgXPGZbYkOA&8U$@KHDBWNMR0Q"

greyASCIICount :: Int
greyASCIICount = length greyASCIIOrder

floatToASCII :: Float -> Char
floatToASCII = (greyASCIIOrder !!).truncate.(fromIntegral greyASCIICount *) 

toASCII :: Int -> Int -> Char
toASCII max val = floatToASCII $ fromIntegral val / fromIntegral (max+1)

skipSpaces :: PGMParser ()
skipSpaces = PGMParser func where
    func ctx = Right ((), L8.dropWhile isSpace ctx)

skipComments :: PGMParser ()
skipComments = PGMParser func where
    func ctx 
        | L8.null ctx = Right((),ctx)
        | L8.head ctx == '#' = Right ((), L8.dropWhile (not.('\n'==)) ctx)
        | otherwise = Right ((), ctx)

cleanInput :: PGMParser ()
cleanInput = skipSpaces ===>& skipComments ===>& skipSpaces

takeChunk :: PGMParser String
takeChunk = PGMParser func where 
    func ctx = Right (L8.unpack (L8.takeWhile (not.isSpace) ctx), L8.dropWhile (not.isSpace) ctx)

takeInt :: PGMParser Int
--takeInt = takeChunk ==> (\x -> PGMParser (\ctx -> Right (read x, ctx)))
takeInt =  do 
    val <- takeChunk
    return $ read val

matchHeader :: String -> PGMParser (Bool)
matchHeader h = takeChunk ==> (\x -> PGMParser (\ctx -> Right (x == h, ctx)))

parseFormat :: PGMParser PGMFormat
--parseFormat = takeChunk ==> (\x -> PGMParser (\ctx -> Right (if x == "P5" then P5 else if x == "P2" then P2 else InvalidFormat, ctx)))
parseFormat = takeChunk ==> (\x -> PGMParser (\ctx -> if x == "P2" then Right (P2, ctx) else Left ("Invalid Format " ++ x)))

rest :: PGMParser L.ByteString
rest = PGMParser (\ctx -> Right (ctx, ctx))



data Greymap = Greymap {
    width :: Int,
    height :: Int,
    max :: Int,
    bin :: L.ByteString
}

instance Show Greymap where
    show (Greymap w h m d) = "Greymap " ++ show w ++ "x" ++ show h ++ " " ++ show m ++ "\n" ++ vals where
        vals = case getVals d of 
            Left s -> s
            Right (a, _) -> intercalate "\n" a
        getVals = runParser $ replicateM h $ do
            line <- replicateM w takeInt
            --line <- replicateM w $ return 5
            return $ map (toASCII m) line

parseGreymap :: L.ByteString -> Greymap
parseGreymap s = Greymap w h m d where 
    (w, h, m, d) = case getVals s of 
        Left _ -> undefined
        Right (a, _) -> a
    getVals = runParser $ do 
        cleanInput
        format <- parseFormat
        w' <- takeInt
        h' <- takeInt
        m' <- takeInt
        d' <- rest
        return (w',h',m',d')

parseBasic :: L.ByteString -> String
parseBasic s = show (getVals s) where 
    getVals = runParse $ do 
        return ()
        w <- takeInt
        return w



main = do
    bs <- L.readFile "feep.pgm"
    let 
        output = show $ parseGreymap bs
    putStrLn output
    --mapM putStrLn $ lines output


