-- file: ch24/Compressor.hs
import Control.Concurrent (forkIO)
import Control.Exception (handle, IOException)
import Control.Monad (forever)
import qualified Data.ByteString.Lazy as L
import System.IO 

-- Provided by the 'zlib' package on http://hackage.haskell.org/
import Codec.Compression.GZip (compress)

main = do
    putStr "Enter a file to compress> "
    hFlush stdout
    maybeLine <- getLine 
    case maybeLine of
      "" -> return ()      -- treat no name as "want to quit"
      name -> do
        let printE = print :: IOException -> IO ()
        handle printE $ do
          content <- L.readFile name
          forkIO (compressFile name content)
          return ()
        main
  where compressFile path = L.writeFile (path ++ ".gz") . compress