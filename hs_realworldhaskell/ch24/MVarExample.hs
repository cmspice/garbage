-- file: ch24/MVarExample.hs
import Control.Concurrent
import System.IO

main = do
    m <- newEmptyMVar
    forkIO $ do
        v <- takeMVar m
        putStrLn ("received: " ++ show v)
    s <- getLine
    putStrLn $ "sending: " ++ s
    putMVar m s  