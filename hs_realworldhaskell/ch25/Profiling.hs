{-
haskell runtime flags:
+RTS
-RTS 
-sstderr 
	output garbage collector performance numbers to stderr
-p
	pretty sure this is enable profiling
-hc
	creates a standard heap profile (.hp file that can be read with hp2ps)
-hy
	allocation by type
-hd
	allocation by constuctors?
-iN
	heap profiling sample rate where in N is number of seconds e.g. 0.01
-K100M 
	set the stack size
compile flags:	
-prof 
	enable profiling
-auto-all
	enable profiling for all top level functions
	{-# SCC "mean" #-} - flags specific function for profiling
-caf-all
	enable profiling for "constant applicative forms", or CAFs
	these are values with no arguments
-fforce-recomp
	force a full recompile
-ddump-simpl
	view core output
-ddump-asm
	view assembly
-funbox-strict-fields
	?
-ON
	? where N is a number
-fvia-C 
	compile to C, then can pass in GCC optimizations -optc-O2

-}


{-# LANGUAGE BangPatterns #-}
-- file: ch25/E.hs
import System.Environment
import Text.Printf
import Control.Parallel.Strategies
import Data.Array.Vector -- for stream fusion

main = do
    [d] <- map read `fmap` getArgs
    printf "%f\n" (mean [1..d])

mean :: [Double] -> Double
mean xs = {-# SCC "mean" #-} sum xs / fromIntegral (length xs)

meanseq :: [Double] -> Double
meanseq xs = s / fromIntegral n
  where
    (n, s)     = foldl' k (0, 0) xs
    k (n, s) x = n `seq` s `seq` (n+1, s+x)


foldl'rnf :: NFData a => (a -> b -> a) -> a -> [b] -> a
foldl'rnf f z xs = lgo z xs
    where
        lgo z []     = z
        lgo z (x:xs) = lgo z' xs
            where
                z' = f z x `using` rnf

meanusingrnf :: [Double] -> Double
meanusingrnf xs = s / fromIntegral n
  where
    (n, s)     = foldl'rnf k (0, 0) xs
    k (n, s) x = (n+1, s+x) :: (Int, Double)

meanbang :: [Double] -> Double
meanbang xs = s / fromIntegral n
  where
    (n, s)       = foldl' k (0, 0) xs
    k (!n, !s) x = (n+1, s+x)

-- a and b always kept in WHNF
data Pair a b = Pair !a !b

meanstrictdata :: [Double] -> Double
meanstrictdata xs = s / fromIntegral n
  where
    Pair n s       = foldl' k (Pair 0 0) xs
    k (Pair n s) x = Pair (n+1) (s+x)

-- -ddump-simpl will show that n is inferred to be of type Integer which can't be unboxed
-- type annotate as Int to make use of unboxed Int

startingPair = (Pair 0 0)::Pair Int Double 

meanstreamfusion :: UArr Double -> Double
meanstreamfusion xs = s / fromIntegral n
  where
    Pair n s       = foldlU k (Pair 0 0) xs
    k (Pair n s) x = Pair (n+1) (s+x)



