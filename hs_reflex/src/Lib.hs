{-# LANGUAGE RecursiveDo #-}

module Lib (
  libMain,
  libTest
) where

import           Relude

import           Control.Monad.Fix

import           Data.Dependent.Sum
import qualified Data.Traversable   as T

import           Reflex
import           Reflex.Host.Class
import           Reflex.Spider


type MyApp t m a = (Reflex t, MonadHold t m, MonadFix m) => Event t () -> m (Event t a)

playReflex ::
  forall a. (Show a)
  => (forall t m. MyApp t m a) -- ^ entry point into reflex app
  -> IO ()
playReflex network =
  runSpiderHost $ do
    (tickEvent,  tickTriggerRef)  <- newEventWithTriggerRef
    tickEventHandle <- subscribeEvent tickEvent

    finalEvent <- runHostFrame $ network tickEvent
    finalEventHandle <- subscribeEvent finalEvent

    -- you could just use this instead
    --fireEventRef
    do
      trig <- readIORef tickTriggerRef
      let
        eventValue :: (MonadReadEvent t m, Show a) => EventHandle t a -> m (Maybe a)
        eventValue = readEvent >=> T.sequenceA
      (ticked, final) <- case trig of
        Nothing -> error "no trigger ref"
        Just t  -> fireEventsAndRead [t :=> Identity ()] $ do
          r1 <- readEvent tickEventHandle
          r2 <- eventValue finalEventHandle
          return (r1, r2)
        --Just t  -> fireEventsAndRead [] $ readEvent tickEventHandle
      case ticked of
        Nothing -> print "tick event never triggered"
        Just _  -> return () --print "tick triggered"
      case final of
        Nothing -> print "no final output"
        Just x  -> print x


test_mergeList_ordering :: IO ()
test_mergeList_ordering = playReflex network where
  network ev = return merged where
    --evs = map (\x -> fmap (const x) ev) [0..10]
    evs = scanl (\acc x -> fmap (const x) acc) (fmap (const 0) ev) [1..10]
    -- events are in the same order as the pre merged list
    merged = mergeList (reverse evs)

test_knot :: IO ()
test_knot = playReflex network where
  network :: forall t m. MyApp t m ()
  network ev = mdo
    let
      -- both these versions cause infinite loop
      addev :: Event t Int
      addev = fmapMaybe (\x -> if x > 10 then Nothing else Just (x + 1)) (updated dynInt)
      --addMethod x = if x > 10 then return Nothing else return (Just (x+1))
      --addev = push addMethod (updated dynInt)
    dynInt <- holdDyn 0 (leftmost [fmap (const 0) ev, addev])
    return myNever

-- test if constDyn fires upon creation
-- expect "no final output" if no event is triggered
test_constDyn :: IO ()
test_constDyn = playReflex network where
  network :: forall t m. MyApp t m ()
  network _ = mdo
    return $ updated $ constDyn ()

-- attempt to sample a value that's about to get updated
-- ensure old value is obtained and no infinite loop
test_sampleLoop :: IO ()
test_sampleLoop = playReflex network where
  network ev = mdo
    let
      foldfn () prev = do
        -- sample previous value of d2
        d2v <- sample . current $ d2
        return $ prev + d2v
    d1 <- foldDynM foldfn 0 ev
    d2 <- holdDyn 100 (updated d1)
    return $ updated d1



myNever :: (Reflex t) => Event t ()
myNever = never

test_holdDyn_initial_event :: IO ()
test_holdDyn_initial_event = do
  let
    network ev = holdDyn () (traceEvent "this does get triggered" ev) >> return (fmap (const "Hi") ev)
  playReflex network



libMain :: IO ()
libMain = test_holdDyn_initial_event

label :: Text -> IO ()
label s = do
  putTextLn $ "\n\n:::::" <> s <> ":::::"

libTest :: IO()
libTest = do
  label "test_holdDyn_initial_event"
  test_holdDyn_initial_event
  label "test_mergeList_ordering"
  test_mergeList_ordering
  --label "test_knot"
  --test_knot
  label "test_constDyn"
  test_constDyn
  label "test_sampleLoop"
  test_sampleLoop
