 
{-# LANGUAGE FlexibleInstances #-}

module Ch2
(
	(==>), TF, any, all, some, every
) where

import Prelude hiding (any, all)

--Logical operators
infix 1 ==>
(==>) :: Bool -> Bool -> Bool
p ==> q = (not p) || q
 
--Truth Function type class
class TF p where
	valid :: p -> Bool
	lequiv :: p -> p -> Bool
	contradiction :: p -> Bool

instance TF Bool where
	valid = id
	lequiv p q = p == q
	contradiction = not.id

instance TF p => TF (Bool -> p) where
	valid f = valid (f True) && valid (f False)
	lequiv f g = lequiv (f True) (g True) && lequiv (f False) (g False)
	contradiction f = (not.valid) (f True) && (not.valid) (f False)

--Quantifiers NOTE these are also defined in Prelude
any :: (a -> Bool) -> [a] -> Bool
any p = or . map p 
all :: (a -> Bool) -> [a] -> Bool
all p = and . map p

some :: [a] -> (a -> Bool) -> Bool
some xs p = Ch2.any p xs
every :: [a] -> (a -> Bool) -> Bool
every xs p = Ch2.all p xs


main = do 
	let 
		test1 = valid True
		test2 = lequiv (\p q -> p ==> q) (\p q -> not q ==> not p)
		test3 = contradiction (\p -> (p == not p))
		test4 = every [1,2,3] (\x -> some [1,2,3] (\y -> x == y))
		test5 = some [1,2,3] (\x -> every [1,2,3] (\y -> x == y))
	mapM (putStrLn.show) [test1, test2, test3, test4, test5]