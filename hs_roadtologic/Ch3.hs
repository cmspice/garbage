import Ch1


sieve :: [Integer] -> [Integer]
sieve (0 : xs) = xs
sieve (n : xs) = n : sieve(filter (\x -> rem x n /= 0) xs)

sievePrimes = sieve [2..]

pdivisors :: Integer -> [Integer]
pdivisors n = filter (\x -> rem n x == 0) [1..(n-1)]

isPerfectNumber x = sum (pdivisors x) == x
perfectNumbers = filter isPerfectNumber [1..]

main = do
	(putStrLn.show) $ take 100 primes