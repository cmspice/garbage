module Ch4
(
	delete, nub, powerList, intersect, genUnion, genIntersect
) where

delete :: Eq a => a -> [a] -> [a]
delete _ [] = []
delete a (x:xs) 
	| a == x = delete a xs
	| otherwise = x : delete a xs
				
nub :: Eq a => [a] -> [a]
nub [] = []
nub (x:xs) = x : (nub $ delete x xs)

powerList :: [a] -> [[a]]
powerList [] = [[]]
powerList (x:xs) = (powerList xs) ++ (map (x:) $ powerList xs)

intersect :: Eq a => [a] -> [a] -> [a]
intersect [] _ = []
intersect _ [] = []
intersect (x:xs) (y:ys) 
	| x == y = x : intersect xs ys
	| otherwise = (intersect [x] ys) ++ (intersect [y] xs) ++ (intersect xs ys)
	

genUnion :: Eq a => [[a]] -> [a]
genUnion []		= []
genUnion (x:xs) = nub $ x ++ genUnion xs

genIntersect :: Eq a => [[a]] -> [a]
genIntersect (x:xs) = foldl intersect x xs


--main = do 
	--(putStrLn.show) $ take 100 primes