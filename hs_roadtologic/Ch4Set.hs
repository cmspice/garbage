module SetEq
(
	Set (..), 
	emptySet,
	isEmpty,
	inSet,
	subSet,
	insertSet,
	deleteSet,
	powerSet,
	--takeSet,
	list2Set,
	--(!!!),
	unionSet,
	intersectSet,
	differenceSet,
) where

import Ch4
import qualified Data.List as List

newtype Set a = Set [a]

instance Eq a => Eq (Set a) where
	a == b = subSet a b && subSet b a

instance Show a => Show (Set a) where
	show (Set a) = show a

emptySet :: Set a
emptySet = Set []

isEmpty :: Eq a => Set a -> Bool
isEmpty (Set a) = a == []

inSet :: Eq a => a -> Set a -> Bool
inSet a (Set b) = List.elem a b

subSet :: Eq a => Set a -> Set a -> Bool
subSet (Set a) (Set b) = (length $ intersect (nub a) (nub b)) == (length (nub a))

insertSet :: Eq a => a -> Set a -> Set a
insertSet a (Set b) 
	| inSet a (Set b) = Set b
	| otherwise 	  = Set (a:b)

deleteSet :: Eq a => a -> Set a -> Set a
deleteSet a (Set b) = Set (delete a b)

powerSet :: Set a -> Set (Set a)
powerSet (Set (xs)) = Set (map Set (powerList xs))

list2Set :: Eq a => [a] -> Set a
list2Set [] = emptySet
list2Set (x:xs) = insertSet x (list2Set xs)

unionSet :: Eq a => Set a -> Set a -> Set a
unionSet (Set (x:xs)) set = unionSet (Set xs) (insertSet x set)

intersectSet :: Eq a => Set a -> Set a -> Set a
intersectSet (Set a) (Set b) = Set (intersect a b)

differenceSet :: Eq a => Set a -> Set a -> Set a
differenceSet set (Set (x:xs)) = differenceSet (deleteSet x set) (Set xs)

