{-# LANGUAGE TemplateHaskell #-}

module CurryN where

import Prelude
import Control.Monad
import Language.Haskell.TH

curryN :: Int -> Q Exp
curryN n = do
  -- newName creates a new var name with no conflicts
  f  <- newName "f"
  -- makes a specific name
  --let f = mkName "f"
  xs <- replicateM n (newName "x")
  let args = map VarP (f:xs)
      ntup = TupE (map VarE xs)
  return $ LamE args (AppE (VarE f) ntup)


genCurries :: Int -> Q [Dec]
genCurries n = forM [1..n] mkCurryDec
  where mkCurryDec ith = do
          cury <- curryN ith
          -- makes a specific name
          let name = mkName $ "curry" ++ show ith
          return $ FunD name [Clause [] (NormalB cury) []]

-- same as above but using lifted variants of syntax constructors
genCurriesLifted :: Int -> Q [Dec]
genCurriesLifted n = forM [1..n] mkCurryDec
  where mkCurryDec ith = funD name [clause [] (normalB (curryN ith)) []]
          where name = mkName $ "curry" ++ show ith

genId :: Q Exp
genId = do
  x <- newName "x"
  lamE [varP x] (varE x)

-- Using quotation brackets, writing the same meta program can be abbreviated much further as
genId' :: Q Exp
genId' = [| \x -> x |]

genId'' :: Q Exp
genId'' = [| $(varE 'id) |]
