{-# LANGUAGE TemplateHaskell #-}

module Main where

import Prelude

import CurryN

import Control.Monad
import Language.Haskell.TH

$(genCurries 10)

testf3 :: (Int,String,Bool) -> String
testf3 (n,s,b) = show n <> show s <> show b

main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
  print $ $(curryN 3) testf3 10 "boop" False
  print $ curry3 testf3 10 "boop" False
