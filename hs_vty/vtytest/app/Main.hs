module Main where

import Graphics.Vty
import Control.Concurrent

main :: IO ()
main = do
    cfg <- standardIOConfig
    vty <- mkVty cfg
    let line0 = string (defAttr ` withForeColor ` green) "first line"
        line1 = string (defAttr ` withBackColor ` blue) "second line"
        line3 = string (defAttr ` withBackColor ` red) "secaoewtuhna"
        img = (line0 <-> line1) <|> line3 <|>(line0 <-> line1)
        pic = picForImage (translate 10 10 img)
        loop = do
          update vty pic
          e <- nextEvent vty
          case e of
            EvResize _ _ -> loop
            _ -> return ()
          --threadDelay 1000
          shutdown vty
          print ("Last event was: " ++ show e)
    loop
