--1.
--how do I do this
class Genotype (n :: Nat) gt | gt -> n where
    geneLength :: gt -> Integer
    geneLength gt = natVal (Proxy::Proxy n)
    geneSum :: DNA -> gt -> Int

--without doing this
class (KnownNat n) => Genotype n gt | gt -> n where
    geneLength :: gt -> Integer
    geneLength gt = natVal (Proxy :: Proxy n)
    geneSum :: DNA -> gt -> Int


--5
--how does haskell keep track of all its evaluated thunks. Doesn't it need to make a massive lookup table? 
--like, if you did a fibbonaci function, does it just create a table of fib evaluated at all indices?


--6 
--how/when does haskell reuse its evaluated thunks? When does it throw them away?
--e.g. if I do something like the following, will it always reuse the results from the first evaluation?
main = do
    putStrLn . show $ somExpensiveFunction
    putStrLn . show $ somExpensiveFunction