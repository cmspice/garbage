
module Ch1.P4
(
  isPalindrome,
  permutations,
  p1_4
) where

import qualified Data.List as L
{-
-- | returns all palindrome that are permutations of the input text
-- assumse input text has no spaces
makePalindromePermutations :: String -> [String]
makePalindromePermutations s
  -- one character is a palindrome
  | length s == 1 = [s]
  -- two of the same character is a palindrome
  | length s == 2 = if s ! 0 == s ! 1 then [s] else []
  | otherwise = r where
    tailsArr [] = []
    tailsArr (x:xs) = xs : tailsArr xs
    sTailsArr = fmap tailsArr s


     -- go through each character from left to right
     -- find a matching character to the right
     -- TODO track which characters we've already searched
     -- remove both characters and recursively call the method
-}

-- | returs True if the input string is a palindrome
isPalindrome :: String -> Bool
isPalindrome s
  -- 1 or 0 length strings are palindromes
  | length s <= 1 = True
  -- check that the outside characters are the same and recurse
  | otherwise = if head s == last s then isPalindrome . tail . init $ s else False

permutations :: Eq a => [a] -> [[a]]
permutations [] = [[]]
permutations s = do
  -- extract every character in s
  c <- s
  -- remove it and find permutations
  p <- permutations $ L.delete c s
  -- add c to the beginning
  return $ c : p

-- | takes a string and returns a list of palindromes that it is a permutation of
-- ignores spaces and returns strings with no spaces
p1_4 :: String -> [String]
p1_4 = filter isPalindrome . permutations . filter (/= ' ')
