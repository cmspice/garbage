{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Ch1.P4Spec where

import Ch1.P4
import Test.QuickCheck
import Test.Hspec
import Control.Monad

import Debug.Trace

import qualified Data.List as L

newtype SizedString = SizedString String deriving (Show)

instance Arbitrary SizedString where
  arbitrary = scale (min 6) (SizedString <$> arbitrary)

-- | check that everything returned by Data.List.permutations is contained in our implementation
prop_permutations :: SizedString -> Bool
prop_permutations (SizedString "") = True
prop_permutations (SizedString s) =
  trace (show (permutations s, L.permutations s)) $ all id $ map (flip elem $ permutations s) (L.permutations s)

-- | convert input string to a palindrome and check against our implementation
prop_isPalindrome :: SizedString -> Bool
prop_isPalindrome (SizedString s) = isPalindrome $ s ++ reverse s

-- Template haskell nonsense to run all properties prefixed with "prop_" in this file
return []
props = $allProperties

-- hspec nonsense
spec = forM_ props (\(s,p) -> it s $ property $ p)
