//https://leetcode.com/problems/add-two-numbers/

struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    // be sure to pass the last node in the linked list
    ListNode* addDigit(ListNode* l, int val) {
        if(l == NULL) {
            return new ListNode(val);
        } else {
            l->next = new ListNode(val);
        }
        return l->next;
    }
    // prints list node in reverse order
    void printListNode(ListNode* l) {
        while(l != NULL) {
            cout << l->val;
            l = l->next;
        }
        cout << endl;
    }
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int carryover = 0;
        ListNode* r = NULL;
        ListNode* last = NULL;
        while(!(l1 == NULL && l2 == NULL)) {
            int digit = carryover;
            if(l1 != NULL) {
                digit += l1->val;
                l1 = l1->next;
            }
            if(l2 != NULL) {
                digit += l2->val;
                l2 = l2->next;
            }
            if (digit > 9) {
                digit = digit % 10;
                carryover = 1;
            } else {
                carryover = 0;
            }
            last = addDigit(last, digit);
            if(r == NULL) {
                r = last;
            }
        }
        if(carryover > 0) {
            last = addDigit(last, carryover);
        }
        return r;
    }
};
