// https://leetcode.com/problems/median-of-two-sorted-arrays/

class Solution {
public:

    // copy pasta
    template<typename T>
    void print(const std::vector<T>& v)
    {
        for (auto i: v) {
            std::cout << i << ' ';
        }
        std::cout << '\n';
    }

    // copy pasta
    // returns [m,n)
    template<typename T>
    std::vector<T> slice(const std::vector<T>& v, int m, int n) {
        auto first = v.cbegin() + m;
        auto last = v.cbegin() + n;

        std::vector<T> vec(first, last);
        return vec;
    }

    vector<int> sliceRight(const vector<int>& v) {
        // if odd
        if(v.size()%2) {
            return slice<int>(v, v.size()/2+1, v.size());
        } else {
            return slice<int>(v, v.size()/2, v.size());
        }
    }

    vector<int> sliceLeft(const vector<int>& v) {
        // if odd
        if(v.size()%2) {
            return slice(v, 0, v.size()/2);
        } else {
            return slice(v, 0, v.size()/2);
        }
    }


    // offset is number of elements not included in the vector (- is to the left, + is to right)
    double median(const vector<int>& nums, int offset) {
        assert(abs(offset) < nums.size());
        int size = nums.size() + offset;
        //assert(size/2.0 > 0 && size/2.0 < nums.size());
        if(size%2) {
            cout << "odd: " << nums[size/2] << endl;
            return (nums[size/2]);
        } else {
            cout << "even: " << nums[size/2-1] << " " << nums[size/2] << endl;
            return (nums[size/2-1] + nums[size/2])/2.0;
        }
    }

    double findMedianSortedArrays(const vector<int>& nums1, const vector<int>& nums2, int o1, int o2) {
        if(nums1.size() == 1 && nums2.size() == 1) {
            return (nums1[0] + nums2[0])/2.0;
        }
        if(nums1.empty()) {
            return median(nums2, o2);
        }
        if(nums2.empty()) {
            return median(nums1, o1);
        }
        double m1 = median(nums1, o1);
        double m2 = median(nums2, o2);

        if(abs(m1-m2) < 0.000001); {
            return m1;
        }

        if(m1 < m2) {
            return findMedianSortedArrays(sliceRight(nums1), sliceLeft(nums2), 0, 0);
        } else {
            return findMedianSortedArrays(sliceLeft(nums1), sliceRight(nums2), 0, 0);
        }
        return 0;

    }

    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        return findMedianSortedArrays(nums1, nums2, 0, 0);
        //testHelpers();
        //return 0;
    }

    void testHelpers() {
        vector<int> v{ 10, 20, 30, 40 };
        vector<int> v2{ 10, 20, 30, 40, 50 };
        assert(median(v,0) == 25);
        assert(median(v,-1) == 20);
        assert(median(v,1) == 30);
        assert(median(v,-2) == 15);

        print<int>(sliceLeft(v));
        print<int>(sliceRight(v));
        print<int>(sliceLeft(v2));
        print<int>(sliceRight(v2));
    }
};








class Solution {
public:

    // offset is number of elements not included in the vector (- is to the left, + is to right)
    double median(const vector<int>& nums, int offset) {
        assert(abs(offset) < nums.size());
        int size = nums.size() + offset;
        //assert(size/2.0 > 0 && size/2.0 < nums.size());
        if(size%2) {
            cout << "odd: " << nums[size/2] << endl;
            return (nums[size/2]);
        } else {
            cout << "even: " << nums[size/2-1] << " " << nums[size/2] << endl;
            return (nums[size/2-1] + nums[size/2])/2.0;
        }
    }

    int eltsDiscarded(int size) {
        // if odd
        if(size%2) {
            return size/2;
        } else {
            return size/2-1;
        }
    }


    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2, int l1, int r1, int l2, int r2) {

        cout << "all: " << nums1.size() << " " << nums2.size() << " " << l1 << " " << r1 << " " << l2 << " " << r2 << endl;

        // if all elements have been truncated
        if(l1+r1 == nums1.size()) {
            return median(nums2, l2-r2);
        }
        if(l2+r2 == nums2.size()) {
            return median(nums1, l1-r1);
        }


        double m1 = median(nums1, l1-r1);
        double m2 = median(nums2, l2-r2);
        //cout << m1 << " " << m2 << " " << abs(m1-m2) << endl;


        int trunc1 = (nums1.size() + l1-r1)/2;
        int trunc2 = (nums2.size() + l2-r2)/2;
        cout << "trunc: " << trunc1 << " " << trunc2 << endl;

        if(m1 < m2) {
            // truncate everything left of m1 in nums1
            // truncate everything right of m2 in nums2
            return findMedianSortedArrays(nums1, nums2, trunc1, r1, l2, trunc2);
        } else {
            // still O(log n + log m) :)
            return findMedianSortedArrays(nums2, nums1, l2, r2, l1, r1);
        }
    }


    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        return findMedianSortedArrays(nums1, nums2, 0, 0, 0, 0);
    }

    void testHelpers() {
        vector<int> v{ 10, 20, 30, 40 };
        vector<int> v2{ 10, 20, 30, 40, 50 };
        assert(median(v,0) == 25);
        assert(median(v,-1) == 20);
        assert(median(v,1) == 30);
        assert(median(v,-2) == 15);

    }
};
