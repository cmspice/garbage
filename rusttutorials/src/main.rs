// using https://learning-rust.github.io/

mod variables;

trait Foo {
    fn bar(&self);
    fn baz(&self) { println!("We called baz."); }
}

struct Player {
    first_name: String,
    last_name: String,
}

impl Player {
    fn new(first_name: String, last_name: String) -> Player {
        Player {
            first_name : first_name,
            last_name : last_name,
        }
    }

    fn full_name(&self) -> String {
        format!("{} {}", self.first_name, self.last_name)
    }
}

// TODO finish trait and generic notes...

fn genericprint<T: std::fmt::Display>(x: T) { // x has type T, T is a generic type
    println!("output : {}", x)
}


struct Point<T> {
  x: T,
  y: T,
}

// how do I make this generic?
impl std::fmt::Display for Point<f32>{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "Point({},{})", self.x, self.y)
    }
}

fn get_id_by_username(username: &str) -> Option<usize> {
    if username == "hi" {
        return Some(1);
    }
    return None;
}

// generics
fn main6() {
    let point_a = Point { x: 0, y: 0 }; // T is a int type
    let point_b = Point { x: 0.0, y: 0.0 }; // T is a float type
    //genericprint(point_a); //didn't implement display
    genericprint(point_b);
}

// Struct Declaration
struct Color {
    red: u8,
    green: u8,
    blue: u8
}

// tuple structs
struct TColor (u8, u8, u8);
struct Kilometers(i32);

// fancy enum decleration
enum FlashMessage {
  Success, //a unit variant
  Warning{ category: i32, message: String }, //a struct variant
  Error(String) //a tuple variant
}

fn print_flash_message(m : FlashMessage) {
  // pattern matching with enum
  match m {
    FlashMessage::Success =>
      println!("Form Submitted correctly"),
    FlashMessage::Warning {category, message} => //Destructure, should use same field names
      println!("Warning : {} - {}", category, message),
    FlashMessage::Error(msg) =>
      println!("Error : {}", msg)
  }
}

// structs and enums
fn main5() {
    println!("main5");

    // creating an instance
    let black = Color {red: 0, green: 0, blue: 0};

    // accessing its fields using dot notation
    println!("Black = rgb({}, {}, {})", black.red, black.green, black.blue); //Black = rgb(0, 0, 0)

    // structs are immutable by default, use `mut` to make it mutable but doesn't support field level mutability
    let mut x = 255;
    let mut link_color = Color {red: 0,green: 0,blue: x};
    link_color.blue = 238;
    println!("Link Color = rgb({}, {}, {}) x={}", link_color.red, link_color.green, link_color.blue, x); //Link Color = rgb(0, 0, 238)

    // copy elements from another instance
    let blue = Color {blue: 255, .. link_color};
    println!("Blue = rgb({}, {}, {})", blue.red, blue.green, blue.blue); //Blue = rgb(0, 0, 255)

    // destructure the instance using a `let` binding, this will not destruct blue instance
    let Color {red: r, green: g, blue: b} = blue;
    println!("Blue = rgb({}, {}, {})", r, g, b); //Blue = rgb(0, 0, 255)

    // tuple sturcts
    {
        // creating an instance
        let black = TColor (0, 0, 0);

        // destructure the instance using a `let` binding, this will not destruct black instance
        let TColor (r, g, b) = black;
        println!("Black = rgb({}, {}, {})", r, g, b); //black = rgb(0, 0, 0);

        //newtype pattern
        let distance = Kilometers(20);
        // destructure the instance using a `let` binding
        let Kilometers(distance_in_km) = distance;
        println!("The distance: {} km", distance_in_km); //The distance: 20 km
    }

    // enums
    {
        let mut form_status = FlashMessage::Success;
        print_flash_message(form_status);

        form_status = FlashMessage::Warning {category: 2, message: String::from("Field X is required")};
        print_flash_message(form_status);

        form_status = FlashMessage::Error(String::from("Connection Error"));
        print_flash_message(form_status);
    }

}

// vec
fn main4() {
    {
        let mut _a:Vec<i32> = Vec::new(); //1.with new() keyword
        let mut _b:Vec<i32> = vec![]; //2.using the vec! macro
        let mut c = vec![1i32, 2, 3, 4, 5];//sufixing 1st value with data type
        let mut _d = vec![0; 10]; //ten zeroes
        c[0] = 5;
        c[1] = 5;
        //c[5] = 2; // runtime error
        println!("{:?}", c);
        c.push(1);
        c.push(2);
        c.pop();
        c[5] = 10;
        println!("{:?}", c);

        let mut e: Vec<i32> = Vec::with_capacity(10);
        println!("Length: {}, Capacity : {}", e.len(), e.capacity()); //Length: 0, Capacity : 10

        // These are all done without reallocating...
        for i in 0..10 {
            e.push(i);
        }
        // ...but this may make the vector reallocate
        e.push(11);
    }

    // for
    {
        let mut v = vec![1, 2, 3, 4, 5];

        for i in &mut v {
            println!("A mutable reference to {}", i);
            *i = 0;
        }

        for i in &v {
            println!("A reference to {}", i);
            // *i = 1; cannot borrow as mutable
        }

        // 😱
        for i in v {
            println!("Take ownership of the vector and its element {}", i);
        }
    }
}

// control
fn main3() {
    // if/then (same as go)
    let x = 1;
    if x > 0 {
        println!("x is greater than 0")
    } else {
        println!("x is not greater than 0")
    }

    // match
    let tshirt_width = 20;
    let tshirt_size = match tshirt_width {
        16 => "S", // check 16
        17 | 18 => "M", // check 17 and 18
        19 ... 21 => "L", // check from 19 to 21 (19,20,21)
        22 => "XL",
        _ => "Not Available",
    };
    println!("{}", tshirt_size); // L

    // match tuple
    let marks_paper_a: u8 = 25;
    let marks_paper_b: u8 = 30;
    let output = match (marks_paper_a, marks_paper_b) {
        (50, 50) => "Full marks for both papers",
        (50, _) => "Full marks for paper A",
        (_, 50) => "Full marks for paper B",
        (x, y) if x > 25 && y > 25 => "Good",
        (_, _) => "Work hard"
    };
    println!("{}", output); // Work hard

    // while
    {
        let mut a = 1;
        while a <= 10 {
            println!("Current value : {}", a);
            a += 1; //no ++ or -- on Rust
        }
    }

    // loop
    let mut b1 = 1;
    'outer_loop: loop { //set label outer_loop
      let mut b2 = 1;
      'inner_loop: loop {
        println!("Current Value : [{}][{}]", b1, b2);
        if b1 == 2 && b2 == 2 {
            break 'outer_loop; // kill outer_loop
        } else if b2 == 5 {
            break;
        }
        b2 += 1;
      }
      b1 += 1;
    }

    // for
    {
        for a in 0..5 { //(a = o; a <10; a++) // 0 to 10(exclusive)
            println!("Current value : {}", a);
        }

        // Working with arrays/vectors
        let group : [&str; 4] = ["Mark", "Larry", "Bill", "Steve"];

        for n in 0..group.len() { //group.len() = 4 -> 0..4 👎 check group.len()on each iteration
          println!("Current Person : {}", group[n]);
        }

        for person in group.iter() { //👍 group.iter() turn the array into a simple iterator
          println!("Current Person : {}", person);
        }
    }
}

// vars
fn main2() {
    println!("main2");

    // chars
    // ⭐️ no "x", only single quotes
    //because of Unicode support, char is not a single byte, but four.
    let _x = 'x';
    let _y = '😎';

    let _num: i64 = i64::max_value();

    // arrays (fixed size)
    {
        let a = [1, 2, 3]; // a[0] = 1, a[1] = 2, a[2] = 3
        let _c: [i32; 0] = []; //[Type; # of elements] -> [] /empty array
        let _d: [i32; 3] = [1, 2, 3];
        let _e = ["my value"; 3]; //["my value", "my value", "my value"];
        println!("{:?}", a); // [1, 2, 3]
        println!("{:#?}", a); // multi-line print
    }

    // tuples
    {
        let a = (1, 1.5, true, 'a', "Hello, world!");
        let _b: (i32, f64) = (1, 1.5);
        println!("{:?}", a); //(1, 1.5, true, 'a', "Hello, world!")
    }

    // slices
    {
        let a: [i32; 4] = [1, 2, 3, 4];//Parent Array

        let _b: &[i32] = &a; //Slicing whole array
        let _c = &a[0..4]; // From 0th position to 4th(excluding)
        let _d = &a[..]; //Slicing whole array

        let _e = &a[1..3]; //[2, 3]
        let _f = &a[1..]; //[2, 3, 4]
        let _g = &a[..3]; //[1, 2, 3]
    }

    // strings
    {
        // &str type
        let _a = "Hello, world."; //a: &'static str
        let _b: &str = "こんにちは, 世界!";

        // String type (heap allocated)
        let _str: String = String::from("Hello");
    }

    // type casting
    {
        let a = 15;
        let _b = (a as f64) / 2.0; //7.5
    }
}


// compile-time constant
const _N: i32 = 5;
// constant with memory address
static _M: i32 = 5;

// function declaration
fn print_sum(a: i32, b: i32)  -> i32 {
    a + b
}

fn main() {
    // immutable vars
    let _a = true;
    let _b: bool = true;
    let (x, y) = (1, 2);

    // mutable var
    let mut _z = 5;
    _z = 6;

    // function var
    let fn1: fn(i32, i32) -> i32  = print_sum;

    let myvar : i32 = 5;

    // anonymous function
    let square = |x: i32| -> i32 {
        x * x
    };

    // one liner version
    let squareshort = |x| x * x;
    let rslt = squareshort(myvar);


    println!("Hello, world! {} {} {}", fn1(x, y), rslt, square(10));

    main2();
    main3();
    main4();
    main5();
    main6();

    variables::test();
}
